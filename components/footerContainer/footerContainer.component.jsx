import { makeStyles } from "@material-ui/core/styles";
import { Grid, Box, Typography, Divider } from "@material-ui/core";
import { footerData } from "./footer.data";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
const useStyle = makeStyles(theme => ({
  root: {
    background: "#321539",
    color: "white",
    padding: 50
    // height: "100vh"
  },
  aboutGrid: {
    // padding: 75,
    display: "flex",
    alignItems: "flex-start"
  },
  linksGrid: {
    display: "flex",
    // padding: 75,
    justifyContent: "space-around",
    alignItems: "flex-start"
  },
  tutorComponent: {
    display: "flex",
    alignItems: "space-around",
    // background: "black",
    justifyContent: "space-around",
    flexDirection: "column"
  },

  tutor: {
    padding: 10,
    cursor: "pointer"
  },
  link: {
    padding: 10,
    cursor: "pointer"
  },
  divider: {
    background: "white",
    marginTop: 10
  }
  //
}));
const FooterContainer = () => {
  const classes = useStyle();
  return (
    <Box className={classes.root}>
      <Grid container justify="center" alignItems="flex-start">
        <Grid item className={classes.aboutGrid} md={6} sm={12}>
          <img src="/logoLight.svg" alt="" style={{ marginRight: 15 }} />
          <Typography variant="body2" component="h2">
            Vidyalai is an online tutoring platform with qualified teachers from
            top universities across the globe. We provide live One-on-One
            classes, which take place on our online classroom, which has an
            online video chat and a shared whiteboard. The teacher and the
            student can interact with each other through the video chat and
            write on the shared whiteboard Vidyalai gives you access to the best
            teachers in the world, without any constraints on geography. Once
            you request a lesson, we will match you to a highly qualified
            personal tutor, who will take the first lesson. Once you are
            satisfied with the first lesson, your dedicated academic counsellor
            will chart a detailed academic plan, tailor made for you. We provide
            classes for high school (IB, IGCSE, India, US, UK and Australian
            curricula) and college students, for all subjects. The teachers,
            apart from teaching concepts, will also mentor the students
            personally in academic and co-curricular spheres.
          </Typography>
        </Grid>
        <Grid item className={classes.linksGrid} md={6} sm={12}>
          <Box className={classes.tutorComponent}>
            {footerData.tutoring.map((tutor, idx) => (
              <Typography
                key={idx}
                variant="body1"
                component="h2"
                className={classes.tutor}
              >
                {tutor.lable}
              </Typography>
            ))}
          </Box>
          <Box>
            {footerData.links.map((link, idx) => (
              <Typography
                key={idx}
                variant="body1"
                component="h2"
                className={classes.link}
              >
                {link.lable}
              </Typography>
            ))}
          </Box>
        </Grid>
      </Grid>
      {/*  */}
      <Divider className={classes.divider}></Divider>
      <Grid
        container
        justify="space-between"
        alignItems="center"
        style={{ paddingTop: 15 }}
      >
        <Grid item md={3} sm={12}>
          +91-63741-11932
        </Grid>
        <Grid item md={3} sm={12}>
          <Box>
            <Typography>help@vidyalai.com</Typography>
          </Box>
        </Grid>
        <Grid item md={2} sm={12}>
          <Box display="flex" justifyContent="space-around">
            <FacebookIcon fontSize="large"></FacebookIcon>
            <TwitterIcon fontSize="large"></TwitterIcon>
            <LinkedInIcon fontSize="large"></LinkedInIcon>
            <InstagramIcon fontSize="large"></InstagramIcon>
          </Box>
        </Grid>
        <Grid item md={4} sm={12}>
          <Box display="flex" justifyContent="flex-end">
            <img src="/googlePlayStore.svg" alt="" />
            <img src="/appleAppStore.svg" alt="" />
          </Box>
        </Grid>
      </Grid>
      <Typography
        variant="caption"
        component="h2"
        align="center"
        style={{ paddingTop: 10 }}
      >
        Vidyalai © 2019 All rights reserved.
      </Typography>
    </Box>
  );
};
export default FooterContainer;

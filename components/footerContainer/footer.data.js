export const footerData = {
  links: [
    {
      lable: "Blog"
    },
    {
      lable: "About Us"
    },
    {
      lable: "Educators"
    },
    {
      lable: "FAQs"
    },
    {
      lable: "Terms and Conditons"
    },
    {
      lable: "Privacy Policy"
    },
    {
      lable: "Refund Policy"
    },
    {
      lable: "Sitemap"
    }
  ],
  tutoring: [
    {
      lable: "IB Tutoring"
    },
    {
      lable: "IGCSE Tutoring"
    },
    {
      lable: "CBSE Tutoring"
    },
    {
      lable: "ICSE Tutoring"
    },
    {
      lable: "JEE Tutoring"
    },
    {
      lable: "A Levels Tutoring"
    },
    {
      lable: "SAT Tutoring"
    },
    {
      lable: "AP Tutoring"
    }
  ]
};

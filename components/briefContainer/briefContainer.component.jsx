import { makeStyles } from "@material-ui/core/styles";
import { Typography, Box, Grid, Link, Divider } from "@material-ui/core";
// import { makeStyles } from "@material-ui/styles";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
const useStyle = makeStyles(theme => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 75,
    marginBottom: 75
  },
  leftGrid: {
    paddingLeft: 50,
    paddingRight: 50
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "#0E46B0"
  },
  rightGrid: {},
  link: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    fontSize: 18
  },
  divider: {
    color: "blue",
    marginBottom: 10,
    marginTop: 20
  },
  img: {
    width: "90%"
  }
}));

const BriefContainer = () => {
  const classes = useStyle();
  return (
    <Box className={classes.root}>
      <Grid container justify="center" alignItems="center">
        <Grid item className={classes.leftGrid} md={6} sm={12}>
          <Box className={classes.title}>
            <Typography variant="h4" component="h2" gutterBottom>
              Learn from the best teachers from across the world sitting at home
            </Typography>
            <img src="/teacherStudentRounded.svg" alt="" />
          </Box>

          <Typography component="h2" variant="body1" gutterBottom align="right">
            Vidyalai's teachers are selected after a rigourous interview process
            - only the best, experienced teachers make the final cut. Our IB and
            IGCSE tutors are experts in the curricula and have extensive
            experience in training students from all over the world. We offer
            live one to one classes, tailor made to your requirements, ensuring
            that you get the teacher's undivided attention. From catching up on
            a topic to get and extra advantage over your peers, our classes will
            help you go the extra mile. Our state of the art online classrooms
            makes learning fun and engaing, and ensures that your learning
          </Typography>
          <Divider className={classes.divider}></Divider>
          <Typography>
            <Link href="#" className={classes.link}>
              Check out the online classroom
              <ArrowForwardIcon></ArrowForwardIcon>
            </Link>
          </Typography>
        </Grid>
        <Grid item className={classes.rightGrid} md={6} sm={12}>
          <img src="/classroomPreview1.png" alt="" className={classes.img} />
        </Grid>
      </Grid>
    </Box>
  );
};
export default BriefContainer;

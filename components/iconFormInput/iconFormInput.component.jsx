import AccountCircle from "@material-ui/icons/AccountCircle";
import { makeStyles } from "@material-ui/core/styles";
import {
  Grid,
  FormGroup,
  TextField,
  FormHelperText,
  Box
} from "@material-ui/core";
import MailIcon from "@material-ui/icons/Mail";
import PhoneIcon from "@material-ui/icons/Phone";
import LockIcon from "@material-ui/icons/Lock";
const useStyle = makeStyles({
  formGroup: {
    position: "relative",
    marginBottom: 6,
    marginTop: 6
  },
  TwoFieldFormGroup: {
    marginBottom: 6,
    marginTop: 6
  },
  helperText: {
    position: "absolute",
    bottom: -13,
    width: 300
  },
  twoFieldHelperText: {
    position: "absolute",
    bottom: -5,
    left: 3,
    width: 300
  },
  loginFrom: {
    margin: 25
  },
  icons: {
    position: "absolute",
    top: "55%",
    left: -35,
    transform: "translateY(-50%)",
    color: "#004E92"
    // margin: 10
  }
});
const IconFormInput = ({
  label,
  isError,
  errorVal,
  handleInputField,
  value,
  name,
  restErrorField,
  onBlurField
}) => {
  const classes = useStyle();
  return (
    <FormGroup className={classes.formGroup}>
      {name === "password" ? (
        <MailIcon className={classes.icons}></MailIcon>
      ) : (
        <LockIcon className={classes.icons}></LockIcon>
      )}
      <TextField
        label={label}
        value={value}
        onChange={handleInputField}
        margin="dense"
        variant="outlined"
        name={name}
        error={isError}
        onFocus={() => restErrorField(name)}
        onBlur={() => onBlurField(name)}
      />
      <FormHelperText error={isError} className={classes.helperText}>
        {errorVal}
      </FormHelperText>
    </FormGroup>
  );
};

export const IconTwoFieldInput = ({
  error,
  handleInputField,
  value,
  restErrorField,
  onBlurField
}) => {
  const classes = useStyle();
  return (
    <Box className={classes.formGroup}>
      <PhoneIcon className={classes.icons} />

      <Grid container spacing={1} style={{ position: "relative" }}>
        <Grid item xs={3}>
          <FormGroup className={classes.formGroup}>
            <TextField
              label="Code"
              margin="dense"
              variant="outlined"
              value={value.code}
              name="code"
              onChange={handleInputField}
              error={error.code ? true : false}
              onFocus={() => restErrorField("code")}
              onBlur={() => onBlurField("code")}
            />
            <FormHelperText
              error={error.code ? true : false}
              className={classes.helperText}
            >
              {error.code}
            </FormHelperText>
          </FormGroup>
        </Grid>
        <Grid item xs={9}>
          <FormGroup className={classes.TwoFieldFormGroup}>
            <TextField
              label="Phone Number"
              margin="dense"
              variant="outlined"
              style={{ width: "100%" }}
              value={value.phone}
              name="phone"
              onChange={handleInputField}
              error={error.phone ? true : false}
              onFocus={() => restErrorField("phone")}
              onBlur={() => onBlurField("phone")}
            />
            <FormHelperText
              error={error.phone ? true : false}
              className={classes.twoFieldHelperText}
            >
              {error.phone}
            </FormHelperText>
          </FormGroup>
        </Grid>
      </Grid>
    </Box>
  );
};
// export IconTwoFieldInput;
export default IconFormInput;

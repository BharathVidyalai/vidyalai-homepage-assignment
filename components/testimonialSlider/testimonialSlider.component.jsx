import React, { Component } from "react";
import Slider from "react-slick";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import {
  Paper,
  Grid,
  Button,
  Avatar,
  Box,
  Fab,
  Typography
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import LeftQuote from "../testimonialContainer/leftQuote";

const style = theme => ({
  root: {
    position: "relative",
    margin: "auto",
    width: "65%"
  },

  cardWrapper: {
    width: "96%",
    // height: "50%",
    borderRadius: 20,
    marginLeft: 7
  },
  userProfileContainer: {
    background: `linear-gradient(326.59deg, #45CFE2 -1.77%, #2196F3 51.27%, #3959FA 100%)`,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    color: "white",
    [theme.breakpoints.down("sm")]: {
      // display: "none",
      borderRadius: 20
    }
  },
  //
  avatar: {
    width: 125,
    height: 125,
    borderRadius: "50%",
    backgroundSize: "contain"
  },

  userQuoteContainer: {
    // height: 500,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "column",
    padding: "30px 40px 40px 20px"
  },
  leftQuote: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 85,
    height: 85,
    borderRadius: "50%",
    background: "#D6FACF",
    marginBottom: 50
  },
  quote: {
    marginBottom: 35
  },
  nextButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    transfrom: "translateY(-50%)",
    left: -60,
    zIndex: 2,
    background: "white"
  },
  previousButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    right: -60,
    transfrom: "translateY(-50%)",
    zIndex: 2,
    background: "white"
  }
});

class TestimonialSlider extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const settings = {
      className: "center-content",
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerPadding: "5px"
    };

    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Fab size="small" onClick={this.next} className={classes.nextButton}>
          <ChevronLeftIcon />
        </Fab>
        <Box>
          <Slider {...settings} ref={c => (this.slider = c)}>
            {this.props.testimonials.map((value, idx) => (
              <div className={classes.item} key={idx}>
                <Paper className={classes.cardWrapper}>
                  <Grid container className={classes.container}>
                    <Grid
                      item
                      className={classes.userProfileContainer}
                      md={4}
                      xs={12}
                    >
                      <div
                        style={{ backgroundImage: `url(${value.imgUrl})` }}
                        className={classes.avatar}
                      ></div>
                      <Typography variant="h6" component="h2">
                        {value.name}
                      </Typography>
                      <Typography variant="body1" component="h2">
                        {value.type}
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      md={8}
                      sm={12}
                      className={classes.userQuoteContainer}
                    >
                      <Box className={classes.leftQuote}>
                        <LeftQuote></LeftQuote>
                      </Box>
                      <Typography
                        variant="body1"
                        component="h2"
                        className={classes.quote}
                      >
                        "{value.quote}"
                      </Typography>
                      <Button variant="text" color="primary">
                        Read more on Google
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </div>
            ))}
          </Slider>
        </Box>

        <Fab
          size="small"
          onClick={this.previous}
          className={classes.previousButton}
        >
          <ChevronRightIcon />
        </Fab>
      </div>
    );
  }
}
export default withStyles(style)(TestimonialSlider);

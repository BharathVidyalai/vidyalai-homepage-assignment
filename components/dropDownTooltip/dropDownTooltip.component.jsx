import {
  Tooltip,
  List,
  ListItem,
  ListItemText,
  Button,
  Typography,
  Box,
  Grow
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}
const useStyle = makeStyles({
  dropDownContainer: {
    background: "white",
    color: "black",
    padding: 0
  },
  arrow: {
    display: "block",
    position: "absolute",
    top: -3,
    left: "30%",
    width: 25,
    height: 25,
    background: "white",
    transform: "translateX(-50%)",
    transform: "rotate(45deg)",
    zIndex: -1
  }
});
const DropDownListItems = ({ menuItems }) => {
  const classes = useStyle();
  return (
    <Box boxShadow={3}>
      <List component="nav">
        <Box className={classes.arrow}></Box>

        {menuItems.map((item, idx) => (
          <ListItemLink href="#simple-list" key={idx}>
            <ListItemText primary={item} />
          </ListItemLink>
        ))}
      </List>
    </Box>
  );
};
const DropDownTooltip = ({ label, menuItems }) => {
  const classes = useStyle();
  return (
    <Tooltip
      title={<DropDownListItems menuItems={menuItems}></DropDownListItems>}
      interactive
      classes={{ tooltip: classes.dropDownContainer }}
    >
      <Typography variant="body1" component="h2">
        {label}
      </Typography>
    </Tooltip>
  );
};

export default DropDownTooltip;

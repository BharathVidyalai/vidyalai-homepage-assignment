import {
  Drawer,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  List,
  Collapse
} from "@material-ui/core";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import CollectionsBookmarkIcon from "@material-ui/icons/CollectionsBookmark";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import ContactSupportIcon from "@material-ui/icons/ContactSupport";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import { makeStyles } from "@material-ui/core/styles";

import DraftsIcon from "@material-ui/icons/Drafts";
import SendIcon from "@material-ui/icons/Send";

const useStyle = makeStyles(theme => ({
  list: {
    width: 250
  },
  nested: {
    paddingLeft: 80
  }
}));
const SideBar = ({ sideNavState, toggleSideNav, courses }) => {
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };
  const SideList = () => {
    const { list, nested } = useStyle();
    return (
      <div
        className={list}
        // onClick={toggleSideNav(false)}
      >
        <List component="nav">
          <ListItem button onClick={handleClick}>
            <ListItemIcon>
              <CollectionsBookmarkIcon />
            </ListItemIcon>
            <ListItemText primary="Courses" />
            {open ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {courses.map((course, id) => (
                <ListItem button className={nested} key={id}>
                  <ListItemText primary={course} />
                </ListItem>
              ))}
              <ListItem button className={nested}>
                <ListItemText primary="IGCSE" />
              </ListItem>
              <ListItem button className={nested}>
                <ListItemText primary="" />
              </ListItem>
            </List>
          </Collapse>
          <ListItem button>
            <ListItemIcon>
              <LibraryBooksIcon />
            </ListItemIcon>
            <ListItemText primary="Blog" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <ContactSupportIcon />
            </ListItemIcon>
            <ListItemText primary="Contact Us" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <PersonAddIcon />
            </ListItemIcon>
            <ListItemText primary="Become a Teacher" />
          </ListItem>
          <Divider></Divider>
          <ListItem button>
            <ListItemIcon>
              <VerifiedUserIcon />
            </ListItemIcon>
            <ListItemText primary="Login" />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <PersonAddIcon />
            </ListItemIcon>
            <ListItemText primary="SignUp" />
          </ListItem>
        </List>
      </div>
    );
  };
  return (
    <Drawer
      anchor="right"
      open={sideNavState}
      onClose={() => toggleSideNav(false)}
    >
      <SideList></SideList>
    </Drawer>
  );
};
export default SideBar;

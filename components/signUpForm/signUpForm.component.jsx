import {
  Typography,
  Grid,
  Paper,
  Button,
  AppBar,
  Tabs,
  Tab,
  Box
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
// import { string, object } from 'Joi';
import * as Joi from "@hapi/joi";
import IconFormInput, {
  IconTwoFieldInput
} from "../iconFormInput/iconFormInput.component";

const style = theme => ({
  root: {
    background: `linear-gradient(
            116.83deg,
            #03256c 0%,
            #0846b0 100%,
            #0846b0 100%
          )`,
    color: "white",
    width: "85%"
  },
  //
  form: {
    width: "50%",
    marginLeft: 40,
    marginRight: 40,
    marginTop: 10,
    [theme.breakpoints.down("sm")]: {
      width: "70%"
    }
  },
  //
  loginFrom: {
    width: 500,
    margin: 25,
    paddingTop: 40,
    paddingBottom: 30,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      width: "85%"
    }
  },
  otherSignOptions: {
    display: "flex",
    width: "100%",
    justifyContent: "space-between"
  },
  otherSignButton: {
    padding: 10,
    display: "flex",
    alignItems: "center",
    cursor: "pointer"
  },
  otherSignInImage: {
    marginRight: 10,
    width: 28,
    heigth: 28
  }
});
class SignUpForm extends React.Component {
  state = {
    inputField: { email: "", code: "+91", phone: "", password: "" },
    error: {},
    userClicked: {
      email: false,
      code: false,
      phone: false,
      password: false
    },
    loginType: "student"
  };

  validation = inputField => {
    const schema = Joi.object({
      email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),
      code: Joi.string().required(),
      phone: Joi.string().required(),
      password: Joi.string()
        .min(6)
        .required()
    });
    const options = {
      abortEarly: false
    };
    let error = {};
    let validation = schema.validate(inputField, options);
    if (!validation.error) {
      return error;
    }

    validation.error.details.map(err => {
      error[err.context.key] = err.message;
    });
    return error;
  };
  handleDisplayError = (totalError, userClicked) => {
    let error = {};
    for (let [key, value] of Object.entries(totalError)) {
      userClicked[key] ? (error[key] = value) : null;
    }
    return error;
  };
  handleInputField = e => {
    const { value, name } = e.currentTarget;
    const userClicked = { ...this.state.userClicked };
    userClicked[name] = true;
    let inputField = { ...this.state.inputField };
    inputField[name] = value;
    this.setState({ inputField });
    let totalError = this.validation(inputField);
    let error = this.handleDisplayError(totalError, userClicked);
    this.setState({ error, userClicked });
  };

  restErrorField = name => {
    let { error } = this.state;
    error[name] = "";
    this.setState({ error });
  };
  onBlurField = name => {
    const userClicked = { ...this.state.userClicked };
    userClicked[name] = true;
    console.log(userClicked);
    let totalError = this.validation(this.state.inputField);
    let error = this.handleDisplayError(totalError, userClicked);
    console.log(totalError);
    this.setState({ error, userClicked });
  };
  handleLoginType = (e, value) => {
    this.setState({ loginType: value });
  };
  render() {
    const { classes } = this.props;
    const { email, code, phone, password } = this.state.inputField;
    return (
      <Paper className={classes.loginFrom}>
        <Typography component="h2" variant="h6" color="primary" align="center">
          Join Vidyalai as a
        </Typography>

        <Tabs
          value={this.state.loginType}
          onChange={this.handleLoginType}
          indicatorColor="primary"
        >
          <Tab label="Student" value="student" />
          <Tab label="Teacher" value="teacher" />
        </Tabs>
        <form className={classes.form}>
          <IconFormInput
            label="Email"
            name="email"
            isError={this.state.error.email ? true : false}
            errorVal={this.state.error.email}
            value={email}
            handleInputField={this.handleInputField}
            restErrorField={this.restErrorField}
            onBlurField={this.onBlurField}
          ></IconFormInput>
          <IconTwoFieldInput
            handleInputField={this.handleInputField}
            value={this.state.inputField}
            error={this.state.error}
            handle
            restErrorField={this.restErrorField}
            onBlurField={this.onBlurField}
          ></IconTwoFieldInput>
          <IconFormInput
            label="Password"
            name="password"
            isError={this.state.error.password ? true : false}
            errorVal={this.state.error.password}
            handleInputField={this.handleInputField}
            value={password}
            restErrorField={this.restErrorField}
            onBlurField={this.onBlurField}
          ></IconFormInput>
          <Typography
            variant="caption"
            component="h2"
            align="center"
            style={{ marginTop: 15 }}
          >
            By signing up, you accept the Terms and Privacy Policy
          </Typography>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            style={{ marginTop: 20 }}
          >
            Create Your Account
          </Button>
          <Typography
            variant="caption"
            component="h2"
            align="center"
            style={{ paddingTop: 15, paddingBottom: 20 }}
          >
            or sign up with
          </Typography>
          <Box className={classes.otherSignOptions}>
            <Paper className={classes.otherSignButton} elevation={5}>
              <img
                src="/google-48.png"
                alt=""
                className={classes.otherSignInImage}
              />
              <Typography variant="body2" component="h2">
                Google
              </Typography>
            </Paper>
            <Paper className={classes.otherSignButton} elevation={5}>
              <img
                src="/facebook.png"
                alt=""
                className={classes.otherSignInImage}
              />
              <Typography variant="body2" component="h2">
                Facebook
              </Typography>
            </Paper>
          </Box>
          <Typography
            variant="caption"
            component="h2"
            align="center"
            style={{ paddingTop: 15, paddingBottom: 20 }}
          >
            Already have an account? Login
          </Typography>
        </form>
      </Paper>
    );
  }
}
export default withStyles(style)(SignUpForm);

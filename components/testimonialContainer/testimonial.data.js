export const testimonialData = {
  testimonials: [
    {
      name: "Vinish Pal Singh",
      imgUrl: "/vansh.jpg",
      type: "Parent of Vansh IB Grade 10",
      quote:
        "This is an excellent way of teaching kids at the convenience of your home. Also the quality of teachers is very good . The support I received from Ms Shaheem was excellent. My kids performed better with their guidance . Thanks Vidyalai."
    },
    {
      name: "Anil Nair",
      imgUrl: "/anil.jpg",
      type: "Parent",
      quote:
        "They have some of the best Teachers and all of them IITians and bright minds.My son is so happy with the teaching and I have also referred this to some of my colleagues.I would recommend this to any one who is looking for high quality teaching."
    },
    {
      name: "Dr. Vrishali Patil",
      imgUrl: "/vrishali.png",
      type: "Parent",
      quote:
        "My son Advait has been enrolled in Vidyalai for about 4 months, and he finds it very helpful in school. Coming from the US, it was hard finding tutoring for the International Baccalaureate until we learned about Vidyalai. As the classes were held online, it was very convenient and saves travel time. With Vidyalai, Advait was able to get a head start in his academics and got a grasp of the Math, Physics, and Chemistry topics. I would recommend Vidyalai to any parent looking for online tutoring."
    },
    {
      name: "Siddharth",
      imgUrl: "/siddharth.jpg",
      type: "Student",
      quote:
        "We are very pleased with the kind of interaction that has been made possible between the student and the tutor which we believe has led to a clear understanding of various concepts that are dealt with."
    }
  ]
};

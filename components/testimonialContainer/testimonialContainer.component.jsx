import { makeStyles } from "@material-ui/core/styles";
import { Typography, Box } from "@material-ui/core";
import TestimonialSlider from "../testimonialSlider/testimonialSlider.component";
import { testimonialData } from "./testimonial.data";
const useStyle = makeStyles(theme => ({
  root: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    flexDirection: "column",
    margin: "auto",
    background: "#E9F2FC",
    height: 600,
    [theme.breakpoints.down("md")]: {
      paddingTop: 70,
      height: 900
    }
  },
  title: {
    paddingTop: 50,
    paddingBottom: 10,
    fontWeight: 600
  }
}));

const TestimonialContainer = () => {
  const classes = useStyle();
  return (
    <Box className={classes.root}>
      <Typography
        variant="h5"
        component="h2"
        color="primary"
        align="center"
        className={classes.title}
      >
        Testimonial
      </Typography>
      <TestimonialSlider
        testimonials={testimonialData.testimonials}
      ></TestimonialSlider>
    </Box>
  );
};
export default TestimonialContainer;

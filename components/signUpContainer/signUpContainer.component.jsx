import { makeStyles } from "@material-ui/core/styles";
import { Typography, Box, Grid } from "@material-ui/core";
// import SignUpForm from "../components/signUpForm/signUpForm.component";
import SignUpFormComponent from "../signUpForm/signUpForm.component";
const useStyle = makeStyles(theme => ({
  root: {
    background: `linear-gradient(
        116.83deg,
        #03256c 0%,
        #0846b0 100%,
        #0846b0 100%
      )`,
    color: "white",
    width: "100%"
  },
  RightGrid: {
    display: "flex",
    justifyContent: "center"
    // margin: 20
  },
  leftGridTitle: {
    [theme.breakpoints.down("sm")]: {
      paddingTop: 20,
      textAlign: "center"
    }
  },
  leftGridSubTitle: {
    [theme.breakpoints.down("sm")]: {
      paddingBottom: 10,
      textAlign: "center"
    }
  }
}));
const SignUpContainer = () => {
  const classes = useStyle();
  return (
    <Box className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-around"
        alignItems="center"
      >
        <Grid item className={classes.LeftGrid} md={5} sm={12}>
          <Typography
            variant="h4"
            component="h2"
            className={classes.leftGridTitle}
            gutterBottom
          >
            Ready to start learning?
          </Typography>
          <Typography
            variant="h5"
            component="h2"
            className={classes.leftGridSubTitle}
          >
            Create your account now!
          </Typography>
        </Grid>
        <Grid item className={classes.RightGrid} md={7} sm={12}>
          <SignUpFormComponent></SignUpFormComponent>
        </Grid>
      </Grid>
    </Box>
  );
};
export default SignUpContainer;

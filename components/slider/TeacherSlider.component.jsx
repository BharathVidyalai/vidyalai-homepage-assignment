import React, { Component } from "react";
import Slider from "react-slick";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { Fab, Box, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import "./TeacherSlider.style.scss";

const style = theme => ({
  root: {
    position: "relative",
    margin: "auto",
    width: "50%",
    [theme.breakpoints.down("sm")]: {
      width: "70%"
    }
  },
  item: {
    position: "relative",
    alignSelf: "center",
    margin: "auto",
    textAlign: "center"
  },
  //
  img: {
    margin: "auto",
    width: 100,
    height: 100,
    borderRadius: "50%",
    transition: "0.5s"
  },
  nextButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    transfrom: "translateY(-50%)",
    left: -40,
    zIndex: 2,
    background: "white"
  },
  previousButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    right: -40,
    transfrom: "translateY(-50%)",
    zIndex: 2,
    background: "white"
  }
});
class SimpleSlider extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  render() {
    const settings = {
      className: "center-content",
      centerMode: true,
      infinite: true,
      centerPadding: "5px",
      slidesToShow: this.props.isSmall ? 1 : 3,
      speed: 500
    };

    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Fab size="small" onClick={this.next} className={classes.nextButton}>
          <ChevronLeftIcon />
        </Fab>
        <Box>
          <Slider {...settings} ref={c => (this.slider = c)}>
            {this.props.teacher.map((slide, idx) => (
              <div className={classes.item} key={idx}>
                <Box className="slide-content-wrapper"></Box>
                <img src={slide.imgUrl} alt="" className={classes.img} />
                <Typography
                  component="h2"
                  variant="h6"
                  className="slide-content-name"
                >
                  {slide.name}
                </Typography>
                <Typography
                  component="h2"
                  variant="body1"
                  className="slide-content-qualification"
                >
                  {slide.qualification}
                </Typography>
                <Typography
                  component="h2"
                  variant="subtitle1"
                  className="slide-content-special"
                >
                  {slide.special}
                </Typography>
              </div>
            ))}
          </Slider>
        </Box>

        <Fab
          size="small"
          onClick={this.previous}
          className={classes.previousButton}
        >
          <ChevronRightIcon />
        </Fab>
      </div>
    );
  }
}
export default withStyles(style)(SimpleSlider);

export const teachers = [
  {
    name: "Anzifar Abdul Latheef",
    qualification: "BTech,IIT Madras",
    special: "Resident Polymath",
    imgUrl: "/teacher_anzifar.jpg"
  },
  {
    name: "Geetha",
    qualification: "BS, IISC & MS, UPJV France",
    special: "Chemistry Wizard",
    imgUrl: "/teacher_srivatsank.jpg"
  },
  {
    name: "Ajay S Dinesh",
    qualification: "BTech, IIT Madras",
    special: "Math Mage",
    imgUrl: "/teacher_ajay.jpg"
  },
  {
    name: "Vinayak Vinod",
    qualification: "BS/MS Physics , IIT Madras",
    special: "Physics Pundit ",
    imgUrl: "/teacher_vinayak.jpg"
  },
  {
    name: "Rabia Reshmani",
    qualification: "MS, IIT Bombay",
    special: "Mathematics",
    imgUrl: "/teacher_rabia.png"
  },
  {
    name: "Laghavi Sharma",
    qualification: "A2, Spanish, Instituto de Cervantes",
    special: "Spanish",
    imgUrl: "/teacher_laghavi.png"
  }
];

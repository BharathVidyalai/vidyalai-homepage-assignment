import { makeStyles } from "@material-ui/core/styles";
import TeacherSlider from "../slider/TeacherSlider.component";
import { teachers } from "./teachers.data";
import { Box, Typography } from "@material-ui/core";
const useStyle = makeStyles(theme => ({
  root: {
    height: 550
  },
  title: {
    color: "blue",
    fontWeight: 500,
    padding: "50px 50px 0 50px"
  },
  subTitle: {
    padding: 10,
    paddingBottom: 50
  }
}));

const TeachersContainers = ({ isSmall }) => {
  console.log(isSmall);
  const classes = useStyle();

  return (
    <Box className={classes.root}>
      <Box>
        <Typography
          variant="h5"
          component="h2"
          align="center"
          className={classes.title}
        >
          Meet ours Teachers
        </Typography>
        <Typography
          variant="body1"
          component="h2"
          align="center"
          className={classes.subTitle}
        >
          Expert teachers from top universities will guide you in all your
          academic needs
        </Typography>
      </Box>
      <TeacherSlider teacher={teachers} isSmall={isSmall}></TeacherSlider>;
    </Box>
  );
};
export default TeachersContainers;

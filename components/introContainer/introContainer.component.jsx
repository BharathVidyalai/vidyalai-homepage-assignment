import Button from "@material-ui/core/Button";
import Header from "../header/header.component";
import DropDownTooltip from "../dropDownTooltip/dropDownTooltip.component";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, Box, Paper } from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

import { courseData } from "./course.data";
const useStyle = makeStyles(theme => ({
  root: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontFamily: '"Roboto", sans-serif',
    background: `linear-gradient(
    116.83deg,
    #03256c 0%,
    #0846b0 100%,
    #0846b0 100%)`,
    height: "100vh",
    [theme.breakpoints.down("md")]: {
      paddingTop: 150,
      height: "auto"
    }
  },
  leftGrid: {
    display: "flex",
    justifyContent: "center",

    paddingLeft: 50,
    [theme.breakpoints.down("md")]: {
      padding: "0 30px"
    }
  },
  leftGridText: {
    color: "white",
    padding: "15px 0",
    [theme.breakpoints.down("md")]: {
      textAlign: "center"
    }
  },
  leftButton: {
    marginTop: 30,
    background: "green",
    color: "white",
    zIndex: 1,
    borderRadius: "120px",
    [theme.breakpoints.down("md")]: {
      display: "flex",
      margin: "auto"
    }
  },
  rigthGrid: {
    position: "relative",
    paddingTop: 50,
    [theme.breakpoints.down("md")]: {
      paddingBottom: 150
    }
  },
  img: {
    display: "flex",
    margin: "auto",
    height: "auto",
    width: "90%",
    [theme.breakpoints.up("lg")]: {
      width: 600
      // margin: "auto"
    }
  },
  courseContainer: {
    position: "absolute",
    display: "block",
    width: "95%",
    left: "50%",
    transform: "translate(-50%)",
    top: "95%"
  },
  courseBox: {
    display: "flex",
    justifyContent: "space-around",
    flexWrap: "wrap",
    padding: "5px 20px"
  },
  menuItem: {
    display: "flex",
    paddingTop: 12,
    paddingBottom: 12
  },
  buttonIcon: {
    display: "flex",
    alignItems: "center",
    color: "black",
    background: "white",
    borderRadius: "50%",
    padding: 4,
    marginLeft: 10
  }
}));

const IntroPage = ({ isScrolled }) => {
  const classes = useStyle();
  return (
    <div>
      <Header
        isScrolled={isScrolled}
        courses={courseData.courseHeader}
      ></Header>
      <Box className={classes.root}>
        <Grid
          container
          justify="center"
          alignItems="center"
          className={classes.leftGrid}
        >
          <Grid item lg={6} sm={12}>
            <Typography
              variant="h3"
              component="h2"
              className={classes.leftGridText}
              gutterBottom
            >
              Learn live from the best teachers
            </Typography>

            <Typography
              variant="body1"
              component="h2"
              className={classes.leftGridText}
              gutterBottom
            >
              Attend Interactive One-on-One Classes from subject experts from
              top universities like IITs, BITS, IISc and NITs from your home.
            </Typography>
            <Typography
              component="h2"
              variant="body1"
              className={classes.leftGridText}
              style={{ padding: 0 }}
            >
              Request your first lesson today!
            </Typography>
            <Button variant={"contained"} className={classes.leftButton}>
              Request a lessson
              <span className={classes.buttonIcon}>
                <ArrowForwardIcon></ArrowForwardIcon>
              </span>
            </Button>
          </Grid>
          <Grid item lg={6} sm={12} className={classes.rigthGrid}>
            <img src="/Infographic.png" alt="" className={classes.img} />
          </Grid>
        </Grid>
        <Paper className={classes.courseContainer}>
          <Box className={classes.courseBox}>
            {courseData.topics.map((item, idx) => (
              <Box className={classes.menuItem}>
                <ArrowDropDownIcon
                  style={{ color: "#00BCD4" }}
                ></ArrowDropDownIcon>
                <DropDownTooltip
                  key={idx}
                  label={item.label}
                  menuItems={[...item.courses]}
                ></DropDownTooltip>
              </Box>
            ))}
          </Box>
        </Paper>
      </Box>
    </div>
  );
};
export default IntroPage;

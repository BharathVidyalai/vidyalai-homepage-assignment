export const courseData = {
  topics: [
    { label: "IB", courses: ["physics", "mathametics", "Math/ Applied"] },
    {
      label: "IGCSE",
      courses: ["English", "Spanish", "German"]
    },
    {
      label: "AS & A Levels",
      courses: ["English", "Spanish", "German"]
    },
    {
      label: "CBSE",
      courses: ["English", "Spanish", "German"]
    },
    {
      label: "Competitive Exams",
      courses: ["English", "Spanish", "German"]
    },
    {
      label: "College Courses",
      courses: ["English", "Spanish", "German"]
    },
    {
      label: "US Curriculum",
      courses: ["English", "Spanish", "German"]
    },
    {
      label: "Extra Curricular",
      courses: ["English", "Spanish", "German"]
    },
    {
      label: "Languages",
      courses: ["English", "Spanish", "German"]
    }
  ],
  courseHeader: [
    "IB",
    "IGCSE",
    "AS & A Levels",
    "SAT",
    "GCSE",
    "CBSE",
    "JEE",
    "US Curriculum",
    "And more..."
  ]
};

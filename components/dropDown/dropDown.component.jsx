import "./dropDown.style.scss";

const DropDown = ({ title, menuItems }) => {
  return (
    <div className="dropdown-container">
      <div className="link">{title}</div>
      <div className="dropdown">
        <span></span>
        <ul>
          {menuItems.map((value, id) => (
            <li key={id}>{value}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};
export default DropDown;

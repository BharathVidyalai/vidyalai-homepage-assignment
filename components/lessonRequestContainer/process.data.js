export const processData = {
  steps: [
    {
      title: "Request a Lesson",
      content:
        "Tell us what you need help with. We will connect you with an experienced teacher, and set up the first lesson."
    },
    {
      title: "Attend the First Class",
      content:
        "Attend the first lesson and get comfortable with the classroom. If you are not satisfied with the first lesson, the first lesson will not be charged."
    },
    {
      title: "Enjoy Learning!",
      content:
        "Once you are satisfied with the teacher, a weekly schedule will be set up at your convenience. The same teacher will continue taking classes on a weekly basis."
    }
  ]
};

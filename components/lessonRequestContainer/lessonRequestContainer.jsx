import { makeStyles } from "@material-ui/core/styles";
import { Box, Typography, Grid, Button, Divider } from "@material-ui/core";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { processData } from "./process.data";
const useStyle = makeStyles(theme => ({
  root: {
    background: `linear-gradient(
            116.83deg,
            #03256c 0%,
            #0846b0 100%,
            #0846b0 100%)`,
    paddingTop: 25,
    paddingBottom: 45
  },
  header: {
    textAlign: "center",
    marginBottom: 25
  },
  title: { color: "white", paddingTop: 20, paddingBottom: 10 },
  subtitle: { color: "white", paddingBottom: 35 },
  gridRapper: {
    margin: 35
  },
  grid: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    flexDirection: "column"
  },
  circle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 85,
    height: 85,
    background: "white",
    borderRadius: "50%",
    border: "2px solid green"
  },
  gridTitle: {
    color: "#33E5FF",
    paddingTop: 20,
    paddingBottom: 10
  },
  gridContent: {
    color: "white",
    textAlign: "center",
    paddingBottom: 25
  },
  //
  arrowWarper: {
    position: "absolute",
    top: "20%",
    transform: "translateY(-50%)",
    right: -35,
    zIndex: 1000,
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  arrowContainer: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  arrowline: {
    position: "relative",
    height: 2,
    width: 75,
    color: "white",
    background: "white",
    margin: 0,
    padding: 0
  },
  arrow: {
    position: "absolute",
    color: "white",
    right: -12
  },
  button: {
    display: "flex",
    margin: "auto",
    borderRadius: 100
  }
}));
const LessonRequestContainer = () => {
  const classes = useStyle();
  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <Typography variant="h5" component="h2" className={classes.title}>
          How do I attend Lessons on Vidyalai?
        </Typography>
        <Typography variant="body1" component="h2" className={classes.subtitle}>
          Learn how you can attend one-on-one or group classes, tailor made to
          your requirements.
        </Typography>
      </Box>
      <Box className={classes.gridRapper}>
        <Grid container justify="center" alignItems="center">
          {processData.steps.map((step, idx) => (
            <Grid key={idx} item md={4} sm={12} className={classes.grid}>
              <Box
                style={
                  idx + 1 === processData.steps.length
                    ? {
                        display: "none"
                      }
                    : {}
                }
                className={classes.arrowWarper}
              >
                <Box className={classes.arrowContainer}>
                  <Divider
                    className={classes.arrowline}
                    light={false}
                  ></Divider>
                  <ArrowRightIcon className={classes.arrow}></ArrowRightIcon>
                </Box>
              </Box>
              <Box className={classes.circle}>
                <Typography
                  variant="h5"
                  component="h2"
                  style={{ color: "green" }}
                >
                  {idx + 1}
                </Typography>
              </Box>
              <Typography
                variant="h5"
                component="h2"
                className={classes.gridTitle}
                gutterBottom
              >
                {step.title}
              </Typography>
              <Typography
                variant="body2"
                component="h2"
                className={classes.gridContent}
              >
                {step.content}
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Button variant="contained" className={classes.button}>
        Request a Lesson
      </Button>
    </Box>
  );
};
export default LessonRequestContainer;

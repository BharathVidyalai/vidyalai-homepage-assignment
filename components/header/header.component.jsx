import { useState } from "react";

//Material-Ui components
import { Button, IconButton, Box } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import DropDownTooltip from "../dropDownTooltip/dropDownTooltip.component";
import SideBar from "../sidebar/sideBar.component";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles(theme => ({
  root: {
    position: "fixed",
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    transition: "0.5s"
  },
  menuContainer: {
    display: "flex",
    listStyle: "none",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },

  menuItems: {
    fontFamily: theme.typography.body1.fontFamily,
    color: "white",
    padding: 0,
    paddingLeft: 20,
    margin: 0,
    "&:hover": {
      cursor: "pointer"
    },
    "&:first-child": {
      padding: 0
    }
  },
  left: {
    display: "flex",
    alignItems: "center",
    margin: 10
  },
  right: {
    margin: 10
  },
  button: {
    margin: 10,
    paddingRight: 25,
    paddingLeft: 25,
    borderRadius: 120,
    textTransform: "capitalize",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  barIcon: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block",
      color: "white"
    }
  },
  logo: {
    padding: 10
  },
  loginButton: {
    background: "white"
  },
  signUpButton: {
    background: "#23B9CC",
    color: "white"
  },
  list: {
    width: 250
  }
}));

const Header = ({ isScrolled, courses }) => {
  const [sideNavState, setSideNavState] = useState(false);
  const {
    root,
    left,
    right,
    button,
    logo,
    loginButton,
    signUpButton,
    barIcon,
    menuContainer,
    menuItems
  } = useStyle();

  const toggleSideNav = state => {
    setSideNavState(state);
  };
  return (
    <Box
      className={root}
      style={{
        background: isScrolled ? "none" : "white",
        zIndex: 10
      }}
      boxShadow={isScrolled ? 0 : 3}
    >
      <div className={left}>
        <div className={logo}>
          <img
            src={isScrolled ? "/logoLightWithText.svg" : "/logoWithText.svg"}
          ></img>
        </div>
        <ul className={menuContainer}>
          <li
            style={{ color: isScrolled ? "white" : "black" }}
            className={menuItems}
          >
            <DropDownTooltip
              label="Courses"
              menuItems={courses}
            ></DropDownTooltip>
          </li>
          <li
            style={{ color: isScrolled ? "white" : "black" }}
            className={menuItems}
          >
            Blog
          </li>
          <li
            style={{ color: isScrolled ? "white" : "black" }}
            className={menuItems}
          >
            Contact Us
          </li>
          <li
            style={{
              color: isScrolled ? "white" : "black",
              color: "#23B9CC"
            }}
            className={menuItems}
          >
            Become a Teacher
          </li>
        </ul>
      </div>
      <div className={right}>
        <Button
          variant="contained"
          classes={{ root: button, contained: loginButton }}
        >
          Log In
        </Button>
        <Button
          variant="contained"
          classes={{ root: button, contained: signUpButton }}
        >
          Sign Up
        </Button>
        <IconButton aria-label="delete" onClick={() => toggleSideNav(true)}>
          <MenuIcon
            fontSize="inherit"
            className={barIcon}
            style={{ color: isScrolled ? "white" : "black" }}
          />
        </IconButton>
      </div>
      <SideBar
        sideNavState={sideNavState}
        toggleSideNav={toggleSideNav}
        courses={courses}
      ></SideBar>
    </Box>
  );
};

export default Header;

export const whyData = [
  {
    imgUrl: "/learnFromAnywhere.svg",
    title: "Learn From AnyWhere",
    content:
      "Attend classes from anywhere in the world, from the conveinence of your own home. You don’t have to travel anywhtere."
  },
  {
    imgUrl: "/personalAttention.svg",
    title: "Personal Attention",
    content:
      "Each student will be taught and mentored by a single teacher to personalise the learning experience."
  },
  {
    imgUrl: "/safeAndConvenient.svg",
    title: "Safe and Convenient",
    content:
      "Learn from the safety of your home, at any convenient time, without any of the hassles of travel."
  }
];

import { makeStyles } from "@material-ui/core/styles";
import { Typography, Box, Grid } from "@material-ui/core";
import { whyData } from "./why.data";
const useStyle = theme => ({
  root: {}
});
//
const whyContainer = () => {
  return (
    <Box paddingBottom="25px">
      <Typography
        variant="h5"
        component="h2"
        align="center"
        style={{ padding: 40, fontWeight: 500 }}
        color="primary"
      >
        Why Vidyalai?
      </Typography>
      <Grid container>
        {whyData.map((val, idx) => (
          <Grid items key={idx} md={4} sm={12}>
            <Box
              display="flex"
              justifyContent="space-around"
              flexDirection="column"
              style={{ padding: 30 }}
            >
              <img src={val.imgUrl} alt="" style={{ paddingBottom: 15 }} />
              <Typography
                variant="h5"
                gutterBottom
                align="center"
                color="primary"
                style={{ fontWeight: 500 }}
              >
                {val.title}
              </Typography>
              <Typography variant="body1" gutterBottom align="center">
                {val.content}
              </Typography>
            </Box>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};
export default whyContainer;

import React from "react";
import Head from "next/head";
import IntroPage from "../components/introContainer/introContainer.component";
import BriefContainer from "../components/briefContainer/briefContainer.component";
import LessonRequestContainer from "../components/lessonRequestContainer/lessonRequestContainer";
import TeachersContainers from "../components/teachersContainer/teacherContainer.component";
import TestimonialContainer from "../components/testimonialContainer/testimonialContainer.component";
import SignUpFormComponent from "../components/signUpForm/signUpForm.component";
import SignUpContainer from "../components/signUpContainer/signUpContainer.component";
import FooterContainer from "../components/footerContainer/footerContainer.component";
import WhyContainer from "../components/whyContainer/whyContainer.component";
class Index extends React.Component {
  state = {
    scrollPosition: 0,
    innerWidth: 0
  };

  componentDidMount() {
    this.setState({ innerWidth: window.innerWidth });
    window.addEventListener("scroll", this.handleScroll);
  }
  //
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }
  handleScroll = () => {
    let scrollVal = window.pageYOffset;
    this.setState({ scrollPosition: scrollVal });
  };
  render() {
    return (
      <div>
        <Head>
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <link
            rel="stylesheet"
            type="text/css"
            charSet="UTF-8"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
          />
        </Head>
        <style jsx global>{`
          body {
            height: "100%";
            width: "100%";
            margin: 0;
            padding: 0;
            font-family: "Roboto', sans-serif";
          }
        `}</style>
        <IntroPage
          isScrolled={this.state.scrollPosition < 20 ? true : false}
        ></IntroPage>
        <BriefContainer></BriefContainer>
        <LessonRequestContainer></LessonRequestContainer>
        <TeachersContainers
          isSmall={this.state.innerWidth < 815 ? true : false}
        ></TeachersContainers>
        <TestimonialContainer></TestimonialContainer>
        {/* <RequestLesson></RequestLesson> */}
        {/* <SignUpForm></SignUpForm> */}

        <WhyContainer></WhyContainer>
        <SignUpContainer></SignUpContainer>
        <FooterContainer></FooterContainer>
      </div>
    );
  }
}
export default Index;

module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/briefContainer/briefContainer.component.jsx":
/*!****************************************************************!*\
  !*** ./components/briefContainer/briefContainer.component.jsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/icons/ArrowForward */ "@material-ui/icons/ArrowForward");
/* harmony import */ var _material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/bharath/company/assignment1/components/briefContainer/briefContainer.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

 // import { makeStyles } from "@material-ui/styles";


const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 75,
    marginBottom: 75
  },
  leftGrid: {
    paddingLeft: 50,
    paddingRight: 50
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "#0E46B0"
  },
  rightGrid: {},
  link: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    fontSize: 18
  },
  divider: {
    color: "blue",
    marginBottom: 10,
    marginTop: 20
  },
  img: {
    width: "90%"
  }
}));

const BriefContainer = () => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    justify: "center",
    alignItems: "center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    className: classes.leftGrid,
    md: 6,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h4",
    component: "h2",
    gutterBottom: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: undefined
  }, "Learn from the best teachers from across the world sitting at home"), __jsx("img", {
    src: "/teacherStudentRounded.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: undefined
  })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    component: "h2",
    variant: "body1",
    gutterBottom: true,
    align: "right",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: undefined
  }, "Vidyalai's teachers are selected after a rigourous interview process - only the best, experienced teachers make the final cut. Our IB and IGCSE tutors are experts in the curricula and have extensive experience in training students from all over the world. We offer live one to one classes, tailor made to your requirements, ensuring that you get the teacher's undivided attention. From catching up on a topic to get and extra advantage over your peers, our classes will help you go the extra mile. Our state of the art online classrooms makes learning fun and engaing, and ensures that your learning"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Divider"], {
    className: classes.divider,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    href: "#",
    className: classes.link,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66
    },
    __self: undefined
  }, "Check out the online classroom", __jsx(_material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_3___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    },
    __self: undefined
  })))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    className: classes.rightGrid,
    md: 6,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: undefined
  }, __jsx("img", {
    src: "/classroomPreview1.png",
    alt: "",
    className: classes.img,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: undefined
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (BriefContainer);

/***/ }),

/***/ "./components/dropDownTooltip/dropDownTooltip.component.jsx":
/*!******************************************************************!*\
  !*** ./components/dropDownTooltip/dropDownTooltip.component.jsx ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "/home/bharath/company/assignment1/components/dropDownTooltip/dropDownTooltip.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



function ListItemLink(props) {
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["ListItem"], Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    button: true,
    component: "a"
  }, props, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }));
}

const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__["makeStyles"])({
  dropDownContainer: {
    background: "white",
    color: "black",
    padding: 0
  },
  arrow: {
    display: "block",
    position: "absolute",
    top: -3,
    left: "30%",
    width: 25,
    height: 25,
    background: "white",
    transform: "translateX(-50%)",
    transform: "rotate(45deg)",
    zIndex: -1
  }
});

const DropDownListItems = ({
  menuItems
}) => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    boxShadow: 3,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["List"], {
    component: "nav",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.arrow,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: undefined
  }), menuItems.map((item, idx) => __jsx(ListItemLink, {
    href: "#simple-list",
    key: idx,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["ListItemText"], {
    primary: item,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: undefined
  })))));
};

const DropDownTooltip = ({
  label,
  menuItems
}) => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {
    title: __jsx(DropDownListItems, {
      menuItems: menuItems,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: undefined
    }),
    interactive: true,
    classes: {
      tooltip: classes.dropDownContainer
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "body1",
    component: "h2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: undefined
  }, label));
};

/* harmony default export */ __webpack_exports__["default"] = (DropDownTooltip);

/***/ }),

/***/ "./components/footerContainer/footer.data.js":
/*!***************************************************!*\
  !*** ./components/footerContainer/footer.data.js ***!
  \***************************************************/
/*! exports provided: footerData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "footerData", function() { return footerData; });
const footerData = {
  links: [{
    lable: "Blog"
  }, {
    lable: "About Us"
  }, {
    lable: "Educators"
  }, {
    lable: "FAQs"
  }, {
    lable: "Terms and Conditons"
  }, {
    lable: "Privacy Policy"
  }, {
    lable: "Refund Policy"
  }, {
    lable: "Sitemap"
  }],
  tutoring: [{
    lable: "IB Tutoring"
  }, {
    lable: "IGCSE Tutoring"
  }, {
    lable: "CBSE Tutoring"
  }, {
    lable: "ICSE Tutoring"
  }, {
    lable: "JEE Tutoring"
  }, {
    lable: "A Levels Tutoring"
  }, {
    lable: "SAT Tutoring"
  }, {
    lable: "AP Tutoring"
  }]
};

/***/ }),

/***/ "./components/footerContainer/footerContainer.component.jsx":
/*!******************************************************************!*\
  !*** ./components/footerContainer/footerContainer.component.jsx ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _footer_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer.data */ "./components/footerContainer/footer.data.js");
/* harmony import */ var _material_ui_icons_Facebook__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/Facebook */ "@material-ui/icons/Facebook");
/* harmony import */ var _material_ui_icons_Facebook__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Facebook__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_Instagram__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/Instagram */ "@material-ui/icons/Instagram");
/* harmony import */ var _material_ui_icons_Instagram__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Instagram__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_Twitter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/Twitter */ "@material-ui/icons/Twitter");
/* harmony import */ var _material_ui_icons_Twitter__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Twitter__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_icons_LinkedIn__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/icons/LinkedIn */ "@material-ui/icons/LinkedIn");
/* harmony import */ var _material_ui_icons_LinkedIn__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_LinkedIn__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/home/bharath/company/assignment1/components/footerContainer/footerContainer.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  root: {
    background: "#321539",
    color: "white",
    padding: 50 // height: "100vh"

  },
  aboutGrid: {
    // padding: 75,
    display: "flex",
    alignItems: "flex-start"
  },
  linksGrid: {
    display: "flex",
    // padding: 75,
    justifyContent: "space-around",
    alignItems: "flex-start"
  },
  tutorComponent: {
    display: "flex",
    alignItems: "space-around",
    // background: "black",
    justifyContent: "space-around",
    flexDirection: "column"
  },
  tutor: {
    padding: 10,
    cursor: "pointer"
  },
  link: {
    padding: 10,
    cursor: "pointer"
  },
  divider: {
    background: "white",
    marginTop: 10
  } //

}));

const FooterContainer = () => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    justify: "center",
    alignItems: "flex-start",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    className: classes.aboutGrid,
    md: 6,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: undefined
  }, __jsx("img", {
    src: "/logoLight.svg",
    alt: "",
    style: {
      marginRight: 15
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "body2",
    component: "h2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: undefined
  }, "Vidyalai is an online tutoring platform with qualified teachers from top universities across the globe. We provide live One-on-One classes, which take place on our online classroom, which has an online video chat and a shared whiteboard. The teacher and the student can interact with each other through the video chat and write on the shared whiteboard Vidyalai gives you access to the best teachers in the world, without any constraints on geography. Once you request a lesson, we will match you to a highly qualified personal tutor, who will take the first lesson. Once you are satisfied with the first lesson, your dedicated academic counsellor will chart a detailed academic plan, tailor made for you. We provide classes for high school (IB, IGCSE, India, US, UK and Australian curricula) and college students, for all subjects. The teachers, apart from teaching concepts, will also mentor the students personally in academic and co-curricular spheres.")), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    className: classes.linksGrid,
    md: 6,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.tutorComponent,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: undefined
  }, _footer_data__WEBPACK_IMPORTED_MODULE_3__["footerData"].tutoring.map((tutor, idx) => __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    key: idx,
    variant: "body1",
    component: "h2",
    className: classes.tutor,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: undefined
  }, tutor.lable))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86
    },
    __self: undefined
  }, _footer_data__WEBPACK_IMPORTED_MODULE_3__["footerData"].links.map((link, idx) => __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    key: idx,
    variant: "body1",
    component: "h2",
    className: classes.link,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88
    },
    __self: undefined
  }, link.lable))))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Divider"], {
    className: classes.divider,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    justify: "space-between",
    alignItems: "center",
    style: {
      paddingTop: 15
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 102
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    md: 3,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108
    },
    __self: undefined
  }, "+91-63741-11932"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    md: 3,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 111
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 112
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113
    },
    __self: undefined
  }, "help@vidyalai.com"))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    md: 2,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    display: "flex",
    justifyContent: "space-around",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: undefined
  }, __jsx(_material_ui_icons_Facebook__WEBPACK_IMPORTED_MODULE_4___default.a, {
    fontSize: "large",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: undefined
  }), __jsx(_material_ui_icons_Twitter__WEBPACK_IMPORTED_MODULE_6___default.a, {
    fontSize: "large",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: undefined
  }), __jsx(_material_ui_icons_LinkedIn__WEBPACK_IMPORTED_MODULE_7___default.a, {
    fontSize: "large",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 120
    },
    __self: undefined
  }), __jsx(_material_ui_icons_Instagram__WEBPACK_IMPORTED_MODULE_5___default.a, {
    fontSize: "large",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 121
    },
    __self: undefined
  }))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    md: 4,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 124
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    display: "flex",
    justifyContent: "flex-end",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 125
    },
    __self: undefined
  }, __jsx("img", {
    src: "/googlePlayStore.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 126
    },
    __self: undefined
  }), __jsx("img", {
    src: "/appleAppStore.svg",
    alt: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 127
    },
    __self: undefined
  })))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "caption",
    component: "h2",
    align: "center",
    style: {
      paddingTop: 10
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 131
    },
    __self: undefined
  }, "Vidyalai \xA9 2019 All rights reserved."));
};

/* harmony default export */ __webpack_exports__["default"] = (FooterContainer);

/***/ }),

/***/ "./components/header/header.component.jsx":
/*!************************************************!*\
  !*** ./components/header/header.component.jsx ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/icons/Menu */ "@material-ui/icons/Menu");
/* harmony import */ var _material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _dropDownTooltip_dropDownTooltip_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../dropDownTooltip/dropDownTooltip.component */ "./components/dropDownTooltip/dropDownTooltip.component.jsx");
/* harmony import */ var _sidebar_sideBar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sidebar/sideBar.component */ "./components/sidebar/sideBar.component.jsx");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/home/bharath/company/assignment1/components/header/header.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 //Material-Ui components






const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_5__["makeStyles"])(theme => ({
  root: {
    position: "fixed",
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    transition: "0.5s"
  },
  menuContainer: {
    display: "flex",
    listStyle: "none",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  menuItems: {
    fontFamily: theme.typography.body1.fontFamily,
    color: "white",
    padding: 0,
    paddingLeft: 20,
    margin: 0,
    "&:hover": {
      cursor: "pointer"
    },
    "&:first-child": {
      padding: 0
    }
  },
  left: {
    display: "flex",
    alignItems: "center",
    margin: 10
  },
  right: {
    margin: 10
  },
  button: {
    margin: 10,
    paddingRight: 25,
    paddingLeft: 25,
    borderRadius: 120,
    textTransform: "capitalize",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  barIcon: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block",
      color: "white"
    }
  },
  logo: {
    padding: 10
  },
  loginButton: {
    background: "white"
  },
  signUpButton: {
    background: "#23B9CC",
    color: "white"
  },
  list: {
    width: 250
  }
}));

const Header = ({
  isScrolled,
  courses
}) => {
  const {
    0: sideNavState,
    1: setSideNavState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    root,
    left,
    right,
    button,
    logo,
    loginButton,
    signUpButton,
    barIcon,
    menuContainer,
    menuItems
  } = useStyle();

  const toggleSideNav = state => {
    setSideNavState(state);
  };

  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Box"], {
    className: root,
    style: {
      background: isScrolled ? "none" : "white",
      zIndex: 10
    },
    boxShadow: isScrolled ? 0 : 3,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: undefined
  }, __jsx("div", {
    className: left,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108
    },
    __self: undefined
  }, __jsx("div", {
    className: logo,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109
    },
    __self: undefined
  }, __jsx("img", {
    src: isScrolled ? "/logoLightWithText.svg" : "/logoWithText.svg",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110
    },
    __self: undefined
  })), __jsx("ul", {
    className: menuContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: undefined
  }, __jsx("li", {
    style: {
      color: isScrolled ? "white" : "black"
    },
    className: menuItems,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115
    },
    __self: undefined
  }, __jsx(_dropDownTooltip_dropDownTooltip_component__WEBPACK_IMPORTED_MODULE_3__["default"], {
    label: "Courses",
    menuItems: courses,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: undefined
  })), __jsx("li", {
    style: {
      color: isScrolled ? "white" : "black"
    },
    className: menuItems,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 124
    },
    __self: undefined
  }, "Blog"), __jsx("li", {
    style: {
      color: isScrolled ? "white" : "black"
    },
    className: menuItems,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 130
    },
    __self: undefined
  }, "Contact Us"), __jsx("li", {
    style: {
      color: isScrolled ? "white" : "black",
      color: "#23B9CC"
    },
    className: menuItems,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 136
    },
    __self: undefined
  }, "Become a Teacher"))), __jsx("div", {
    className: right,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    variant: "contained",
    classes: {
      root: button,
      contained: loginButton
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 148
    },
    __self: undefined
  }, "Log In"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    variant: "contained",
    classes: {
      root: button,
      contained: signUpButton
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 154
    },
    __self: undefined
  }, "Sign Up"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["IconButton"], {
    "aria-label": "delete",
    onClick: () => toggleSideNav(true),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160
    },
    __self: undefined
  }, __jsx(_material_ui_icons_Menu__WEBPACK_IMPORTED_MODULE_2___default.a, {
    fontSize: "inherit",
    className: barIcon,
    style: {
      color: isScrolled ? "white" : "black"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 161
    },
    __self: undefined
  }))), __jsx(_sidebar_sideBar_component__WEBPACK_IMPORTED_MODULE_4__["default"], {
    sideNavState: sideNavState,
    toggleSideNav: toggleSideNav,
    courses: courses,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 168
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./components/iconFormInput/iconFormInput.component.jsx":
/*!**************************************************************!*\
  !*** ./components/iconFormInput/iconFormInput.component.jsx ***!
  \**************************************************************/
/*! exports provided: IconTwoFieldInput, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconTwoFieldInput", function() { return IconTwoFieldInput; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/icons/AccountCircle */ "@material-ui/icons/AccountCircle");
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_icons_Mail__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/Mail */ "@material-ui/icons/Mail");
/* harmony import */ var _material_ui_icons_Mail__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Mail__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_Phone__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/Phone */ "@material-ui/icons/Phone");
/* harmony import */ var _material_ui_icons_Phone__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Phone__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_Lock__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/Lock */ "@material-ui/icons/Lock");
/* harmony import */ var _material_ui_icons_Lock__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Lock__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/home/bharath/company/assignment1/components/iconFormInput/iconFormInput.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["makeStyles"])({
  formGroup: {
    position: "relative",
    marginBottom: 6,
    marginTop: 6
  },
  TwoFieldFormGroup: {
    marginBottom: 6,
    marginTop: 6
  },
  helperText: {
    position: "absolute",
    bottom: -13,
    width: 300
  },
  twoFieldHelperText: {
    position: "absolute",
    bottom: -5,
    left: 3,
    width: 300
  },
  loginFrom: {
    margin: 25
  },
  icons: {
    position: "absolute",
    top: "55%",
    left: -35,
    transform: "translateY(-50%)",
    color: "#004E92" // margin: 10

  }
});

const IconFormInput = ({
  label,
  isError,
  errorVal,
  handleInputField,
  value,
  name,
  restErrorField,
  onBlurField
}) => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormGroup"], {
    className: classes.formGroup,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: undefined
  }, name === "password" ? __jsx(_material_ui_icons_Mail__WEBPACK_IMPORTED_MODULE_4___default.a, {
    className: classes.icons,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: undefined
  }) : __jsx(_material_ui_icons_Lock__WEBPACK_IMPORTED_MODULE_6___default.a, {
    className: classes.icons,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["TextField"], {
    label: label,
    value: value,
    onChange: handleInputField,
    margin: "dense",
    variant: "outlined",
    name: name,
    error: isError,
    onFocus: () => restErrorField(name),
    onBlur: () => onBlurField(name),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormHelperText"], {
    error: isError,
    className: classes.helperText,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: undefined
  }, errorVal));
};

const IconTwoFieldInput = ({
  error,
  handleInputField,
  value,
  restErrorField,
  onBlurField
}) => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["Box"], {
    className: classes.formGroup,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: undefined
  }, __jsx(_material_ui_icons_Phone__WEBPACK_IMPORTED_MODULE_5___default.a, {
    className: classes.icons,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["Grid"], {
    container: true,
    spacing: 1,
    style: {
      position: "relative"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["Grid"], {
    item: true,
    xs: 3,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormGroup"], {
    className: classes.formGroup,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["TextField"], {
    label: "Code",
    margin: "dense",
    variant: "outlined",
    value: value.code,
    name: "code",
    onChange: handleInputField,
    error: error.code ? true : false,
    onFocus: () => restErrorField("code"),
    onBlur: () => onBlurField("code"),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormHelperText"], {
    error: error.code ? true : false,
    className: classes.helperText,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108
    },
    __self: undefined
  }, error.code))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["Grid"], {
    item: true,
    xs: 9,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormGroup"], {
    className: classes.TwoFieldFormGroup,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["TextField"], {
    label: "Phone Number",
    margin: "dense",
    variant: "outlined",
    style: {
      width: "100%"
    },
    value: value.phone,
    name: "phone",
    onChange: handleInputField,
    error: error.phone ? true : false,
    onFocus: () => restErrorField("phone"),
    onBlur: () => onBlurField("phone"),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormHelperText"], {
    error: error.phone ? true : false,
    className: classes.twoFieldHelperText,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 130
    },
    __self: undefined
  }, error.phone)))));
}; // export IconTwoFieldInput;

/* harmony default export */ __webpack_exports__["default"] = (IconFormInput);

/***/ }),

/***/ "./components/introContainer/course.data.js":
/*!**************************************************!*\
  !*** ./components/introContainer/course.data.js ***!
  \**************************************************/
/*! exports provided: courseData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "courseData", function() { return courseData; });
const courseData = {
  topics: [{
    label: "IB",
    courses: ["physics", "mathametics", "Math/ Applied"]
  }, {
    label: "IGCSE",
    courses: ["English", "Spanish", "German"]
  }, {
    label: "AS & A Levels",
    courses: ["English", "Spanish", "German"]
  }, {
    label: "CBSE",
    courses: ["English", "Spanish", "German"]
  }, {
    label: "Competitive Exams",
    courses: ["English", "Spanish", "German"]
  }, {
    label: "College Courses",
    courses: ["English", "Spanish", "German"]
  }, {
    label: "US Curriculum",
    courses: ["English", "Spanish", "German"]
  }, {
    label: "Extra Curricular",
    courses: ["English", "Spanish", "German"]
  }, {
    label: "Languages",
    courses: ["English", "Spanish", "German"]
  }],
  courseHeader: ["IB", "IGCSE", "AS & A Levels", "SAT", "GCSE", "CBSE", "JEE", "US Curriculum", "And more..."]
};

/***/ }),

/***/ "./components/introContainer/introContainer.component.jsx":
/*!****************************************************************!*\
  !*** ./components/introContainer/introContainer.component.jsx ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Button */ "@material-ui/core/Button");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../header/header.component */ "./components/header/header.component.jsx");
/* harmony import */ var _dropDownTooltip_dropDownTooltip_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../dropDownTooltip/dropDownTooltip.component */ "./components/dropDownTooltip/dropDownTooltip.component.jsx");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_ArrowDropDown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/ArrowDropDown */ "@material-ui/icons/ArrowDropDown");
/* harmony import */ var _material_ui_icons_ArrowDropDown__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ArrowDropDown__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/icons/ArrowForward */ "@material-ui/icons/ArrowForward");
/* harmony import */ var _material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _course_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./course.data */ "./components/introContainer/course.data.js");
var _jsxFileName = "/home/bharath/company/assignment1/components/introContainer/introContainer.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;








const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__["makeStyles"])(theme => ({
  root: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontFamily: '"Roboto", sans-serif',
    background: `linear-gradient(
    116.83deg,
    #03256c 0%,
    #0846b0 100%,
    #0846b0 100%)`,
    height: "100vh",
    [theme.breakpoints.down("md")]: {
      paddingTop: 150,
      height: "auto"
    }
  },
  leftGrid: {
    display: "flex",
    justifyContent: "center",
    paddingLeft: 50,
    [theme.breakpoints.down("md")]: {
      padding: "0 30px"
    }
  },
  leftGridText: {
    color: "white",
    padding: "15px 0",
    [theme.breakpoints.down("md")]: {
      textAlign: "center"
    }
  },
  leftButton: {
    marginTop: 30,
    background: "green",
    color: "white",
    zIndex: 1,
    borderRadius: "120px",
    [theme.breakpoints.down("md")]: {
      display: "flex",
      margin: "auto"
    }
  },
  rigthGrid: {
    position: "relative",
    paddingTop: 50,
    [theme.breakpoints.down("md")]: {
      paddingBottom: 150
    }
  },
  img: {
    display: "flex",
    margin: "auto",
    height: "auto",
    width: "90%",
    [theme.breakpoints.up("lg")]: {
      width: 600 // margin: "auto"

    }
  },
  courseContainer: {
    position: "absolute",
    display: "block",
    width: "95%",
    left: "50%",
    transform: "translate(-50%)",
    top: "95%"
  },
  courseBox: {
    display: "flex",
    justifyContent: "space-around",
    flexWrap: "wrap",
    padding: "5px 20px"
  },
  menuItem: {
    display: "flex",
    paddingTop: 12,
    paddingBottom: 12
  },
  buttonIcon: {
    display: "flex",
    alignItems: "center",
    color: "black",
    background: "white",
    borderRadius: "50%",
    padding: 4,
    marginLeft: 10
  }
}));

const IntroPage = ({
  isScrolled
}) => {
  const classes = useStyle();
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105
    },
    __self: undefined
  }, __jsx(_header_header_component__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isScrolled: isScrolled,
    courses: _course_data__WEBPACK_IMPORTED_MODULE_8__["courseData"].courseHeader,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Box"], {
    className: classes.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grid"], {
    container: true,
    justify: "center",
    alignItems: "center",
    className: classes.leftGrid,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 111
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grid"], {
    item: true,
    lg: 6,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
    variant: "h3",
    component: "h2",
    className: classes.leftGridText,
    gutterBottom: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: undefined
  }, "Learn live from the best teachers"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
    variant: "body1",
    component: "h2",
    className: classes.leftGridText,
    gutterBottom: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 127
    },
    __self: undefined
  }, "Attend Interactive One-on-One Classes from subject experts from top universities like IITs, BITS, IISc and NITs from your home."), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
    component: "h2",
    variant: "body1",
    className: classes.leftGridText,
    style: {
      padding: 0
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 136
    },
    __self: undefined
  }, "Request your first lesson today!"), __jsx(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_1___default.a, {
    variant: "contained",
    className: classes.leftButton,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 144
    },
    __self: undefined
  }, "Request a lessson", __jsx("span", {
    className: classes.buttonIcon,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 146
    },
    __self: undefined
  }, __jsx(_material_ui_icons_ArrowForward__WEBPACK_IMPORTED_MODULE_7___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147
    },
    __self: undefined
  })))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grid"], {
    item: true,
    lg: 6,
    sm: 12,
    className: classes.rigthGrid,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 151
    },
    __self: undefined
  }, __jsx("img", {
    src: "/Infographic.png",
    alt: "",
    className: classes.img,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 152
    },
    __self: undefined
  }))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Paper"], {
    className: classes.courseContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 155
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Box"], {
    className: classes.courseBox,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 156
    },
    __self: undefined
  }, _course_data__WEBPACK_IMPORTED_MODULE_8__["courseData"].topics.map((item, idx) => __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Box"], {
    className: classes.menuItem,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 158
    },
    __self: undefined
  }, __jsx(_material_ui_icons_ArrowDropDown__WEBPACK_IMPORTED_MODULE_6___default.a, {
    style: {
      color: "#00BCD4"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 159
    },
    __self: undefined
  }), __jsx(_dropDownTooltip_dropDownTooltip_component__WEBPACK_IMPORTED_MODULE_3__["default"], {
    key: idx,
    label: item.label,
    menuItems: [...item.courses],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 162
    },
    __self: undefined
  })))))));
};

/* harmony default export */ __webpack_exports__["default"] = (IntroPage);

/***/ }),

/***/ "./components/lessonRequestContainer/lessonRequestContainer.jsx":
/*!**********************************************************************!*\
  !*** ./components/lessonRequestContainer/lessonRequestContainer.jsx ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_icons_ArrowRight__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/icons/ArrowRight */ "@material-ui/icons/ArrowRight");
/* harmony import */ var _material_ui_icons_ArrowRight__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ArrowRight__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _process_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./process.data */ "./components/lessonRequestContainer/process.data.js");
var _jsxFileName = "/home/bharath/company/assignment1/components/lessonRequestContainer/lessonRequestContainer.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  root: {
    background: `linear-gradient(
            116.83deg,
            #03256c 0%,
            #0846b0 100%,
            #0846b0 100%)`,
    paddingTop: 25,
    paddingBottom: 45
  },
  header: {
    textAlign: "center",
    marginBottom: 25
  },
  title: {
    color: "white",
    paddingTop: 20,
    paddingBottom: 10
  },
  subtitle: {
    color: "white",
    paddingBottom: 35
  },
  gridRapper: {
    margin: 35
  },
  grid: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    flexDirection: "column"
  },
  circle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 85,
    height: 85,
    background: "white",
    borderRadius: "50%",
    border: "2px solid green"
  },
  gridTitle: {
    color: "#33E5FF",
    paddingTop: 20,
    paddingBottom: 10
  },
  gridContent: {
    color: "white",
    textAlign: "center",
    paddingBottom: 25
  },
  //
  arrowWarper: {
    position: "absolute",
    top: "20%",
    transform: "translateY(-50%)",
    right: -35,
    zIndex: 1000,
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  arrowContainer: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  arrowline: {
    position: "relative",
    height: 2,
    width: 75,
    color: "white",
    background: "white",
    margin: 0,
    padding: 0
  },
  arrow: {
    position: "absolute",
    color: "white",
    right: -12
  },
  button: {
    display: "flex",
    margin: "auto",
    borderRadius: 100
  }
}));

const LessonRequestContainer = () => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.header,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h5",
    component: "h2",
    className: classes.title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: undefined
  }, "How do I attend Lessons on Vidyalai?"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "body1",
    component: "h2",
    className: classes.subtitle,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95
    },
    __self: undefined
  }, "Learn how you can attend one-on-one or group classes, tailor made to your requirements.")), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.gridRapper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    justify: "center",
    alignItems: "center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101
    },
    __self: undefined
  }, _process_data__WEBPACK_IMPORTED_MODULE_4__["processData"].steps.map((step, idx) => __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    key: idx,
    item: true,
    md: 4,
    sm: 12,
    className: classes.grid,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    style: idx + 1 === _process_data__WEBPACK_IMPORTED_MODULE_4__["processData"].steps.length ? {
      display: "none"
    } : {},
    className: classes.arrowWarper,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.arrowContainer,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Divider"], {
    className: classes.arrowline,
    light: false,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115
    },
    __self: undefined
  }), __jsx(_material_ui_icons_ArrowRight__WEBPACK_IMPORTED_MODULE_3___default.a, {
    className: classes.arrow,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: undefined
  }))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.circle,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 122
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h5",
    component: "h2",
    style: {
      color: "green"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 123
    },
    __self: undefined
  }, idx + 1)), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h5",
    component: "h2",
    className: classes.gridTitle,
    gutterBottom: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 131
    },
    __self: undefined
  }, step.title), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "body2",
    component: "h2",
    className: classes.gridContent,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 139
    },
    __self: undefined
  }, step.content))))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    variant: "contained",
    className: classes.button,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 150
    },
    __self: undefined
  }, "Request a Lesson"));
};

/* harmony default export */ __webpack_exports__["default"] = (LessonRequestContainer);

/***/ }),

/***/ "./components/lessonRequestContainer/process.data.js":
/*!***********************************************************!*\
  !*** ./components/lessonRequestContainer/process.data.js ***!
  \***********************************************************/
/*! exports provided: processData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "processData", function() { return processData; });
const processData = {
  steps: [{
    title: "Request a Lesson",
    content: "Tell us what you need help with. We will connect you with an experienced teacher, and set up the first lesson."
  }, {
    title: "Attend the First Class",
    content: "Attend the first lesson and get comfortable with the classroom. If you are not satisfied with the first lesson, the first lesson will not be charged."
  }, {
    title: "Enjoy Learning!",
    content: "Once you are satisfied with the teacher, a weekly schedule will be set up at your convenience. The same teacher will continue taking classes on a weekly basis."
  }]
};

/***/ }),

/***/ "./components/sidebar/sideBar.component.jsx":
/*!**************************************************!*\
  !*** ./components/sidebar/sideBar.component.jsx ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_icons_MoveToInbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/icons/MoveToInbox */ "@material-ui/icons/MoveToInbox");
/* harmony import */ var _material_ui_icons_MoveToInbox__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_MoveToInbox__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_icons_Mail__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/icons/Mail */ "@material-ui/icons/Mail");
/* harmony import */ var _material_ui_icons_Mail__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Mail__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_icons_ExpandLess__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/ExpandLess */ "@material-ui/icons/ExpandLess");
/* harmony import */ var _material_ui_icons_ExpandLess__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ExpandLess__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_ExpandMore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/ExpandMore */ "@material-ui/icons/ExpandMore");
/* harmony import */ var _material_ui_icons_ExpandMore__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ExpandMore__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_CollectionsBookmark__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/CollectionsBookmark */ "@material-ui/icons/CollectionsBookmark");
/* harmony import */ var _material_ui_icons_CollectionsBookmark__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_CollectionsBookmark__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_icons_LibraryBooks__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/icons/LibraryBooks */ "@material-ui/icons/LibraryBooks");
/* harmony import */ var _material_ui_icons_LibraryBooks__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_LibraryBooks__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/icons/ContactSupport */ "@material-ui/icons/ContactSupport");
/* harmony import */ var _material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_icons_PersonAdd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/icons/PersonAdd */ "@material-ui/icons/PersonAdd");
/* harmony import */ var _material_ui_icons_PersonAdd__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_PersonAdd__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_icons_VerifiedUser__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/icons/VerifiedUser */ "@material-ui/icons/VerifiedUser");
/* harmony import */ var _material_ui_icons_VerifiedUser__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_VerifiedUser__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _material_ui_icons_Drafts__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/icons/Drafts */ "@material-ui/icons/Drafts");
/* harmony import */ var _material_ui_icons_Drafts__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Drafts__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_icons_Send__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/icons/Send */ "@material-ui/icons/Send");
/* harmony import */ var _material_ui_icons_Send__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Send__WEBPACK_IMPORTED_MODULE_13__);
var _jsxFileName = "/home/bharath/company/assignment1/components/sidebar/sideBar.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;













const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_11__["makeStyles"])(theme => ({
  list: {
    width: 250
  },
  nested: {
    paddingLeft: 80
  }
}));

const SideBar = ({
  sideNavState,
  toggleSideNav,
  courses
}) => {
  const [open, setOpen] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  const SideList = () => {
    const {
      list,
      nested
    } = useStyle();
    return __jsx("div", {
      className: list // onClick={toggleSideNav(false)}
      ,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["List"], {
      component: "nav",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      onClick: handleClick,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemIcon"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47
      },
      __self: undefined
    }, __jsx(_material_ui_icons_CollectionsBookmark__WEBPACK_IMPORTED_MODULE_6___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "Courses",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 50
      },
      __self: undefined
    }), open ? __jsx(_material_ui_icons_ExpandLess__WEBPACK_IMPORTED_MODULE_4___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: undefined
    }) : __jsx(_material_ui_icons_ExpandMore__WEBPACK_IMPORTED_MODULE_5___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Collapse"], {
      in: open,
      timeout: "auto",
      unmountOnExit: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["List"], {
      component: "div",
      disablePadding: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: undefined
    }, courses.map((course, id) => __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      className: nested,
      key: id,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 56
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: course,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: undefined
    }))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      className: nested,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "IGCSE",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      className: nested,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: undefined
    })))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemIcon"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: undefined
    }, __jsx(_material_ui_icons_LibraryBooks__WEBPACK_IMPORTED_MODULE_7___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "Blog",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 74
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemIcon"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: undefined
    }, __jsx(_material_ui_icons_ContactSupport__WEBPACK_IMPORTED_MODULE_8___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "Contact Us",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemIcon"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81
      },
      __self: undefined
    }, __jsx(_material_ui_icons_PersonAdd__WEBPACK_IMPORTED_MODULE_9___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "Become a Teacher",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Divider"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86
      },
      __self: undefined
    }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemIcon"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: undefined
    }, __jsx(_material_ui_icons_VerifiedUser__WEBPACK_IMPORTED_MODULE_10___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "Login",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 91
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItem"], {
      button: true,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 93
      },
      __self: undefined
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemIcon"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94
      },
      __self: undefined
    }, __jsx(_material_ui_icons_PersonAdd__WEBPACK_IMPORTED_MODULE_9___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: undefined
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["ListItemText"], {
      primary: "SignUp",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97
      },
      __self: undefined
    }))));
  };

  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__["Drawer"], {
    anchor: "right",
    open: sideNavState,
    onClose: () => toggleSideNav(false),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104
    },
    __self: undefined
  }, __jsx(SideList, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (SideBar);

/***/ }),

/***/ "./components/signUpContainer/signUpContainer.component.jsx":
/*!******************************************************************!*\
  !*** ./components/signUpContainer/signUpContainer.component.jsx ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _signUpForm_signUpForm_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../signUpForm/signUpForm.component */ "./components/signUpForm/signUpForm.component.jsx");
var _jsxFileName = "/home/bharath/company/assignment1/components/signUpContainer/signUpContainer.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

 // import SignUpForm from "../components/signUpForm/signUpForm.component";


const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  root: {
    background: `linear-gradient(
        116.83deg,
        #03256c 0%,
        #0846b0 100%,
        #0846b0 100%
      )`,
    color: "white",
    width: "100%"
  },
  RightGrid: {
    display: "flex",
    justifyContent: "center" // margin: 20

  },
  leftGridTitle: {
    [theme.breakpoints.down("sm")]: {
      paddingTop: 20,
      textAlign: "center"
    }
  },
  leftGridSubTitle: {
    [theme.breakpoints.down("sm")]: {
      paddingBottom: 10,
      textAlign: "center"
    }
  }
}));

const SignUpContainer = () => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    direction: "row",
    justify: "space-around",
    alignItems: "center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    className: classes.LeftGrid,
    md: 5,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h4",
    component: "h2",
    className: classes.leftGridTitle,
    gutterBottom: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: undefined
  }, "Ready to start learning?"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h5",
    component: "h2",
    className: classes.leftGridSubTitle,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: undefined
  }, "Create your account now!")), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    item: true,
    className: classes.RightGrid,
    md: 7,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: undefined
  }, __jsx(_signUpForm_signUpForm_component__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: undefined
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (SignUpContainer);

/***/ }),

/***/ "./components/signUpForm/signUpForm.component.jsx":
/*!********************************************************!*\
  !*** ./components/signUpForm/signUpForm.component.jsx ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/define-properties */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptors */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-descriptor */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/get-own-property-symbols */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/keys */ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_entries__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/entries */ "./node_modules/@babel/runtime-corejs2/core-js/object/entries.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_entries__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_entries__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _hapi_joi__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @hapi/joi */ "@hapi/joi");
/* harmony import */ var _hapi_joi__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_hapi_joi__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _iconFormInput_iconFormInput_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../iconFormInput/iconFormInput.component */ "./components/iconFormInput/iconFormInput.component.jsx");








var _jsxFileName = "/home/bharath/company/assignment1/components/signUpForm/signUpForm.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = _babel_runtime_corejs2_core_js_object_keys__WEBPACK_IMPORTED_MODULE_5___default()(object); if (_babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default.a) { var symbols = _babel_runtime_corejs2_core_js_object_get_own_property_symbols__WEBPACK_IMPORTED_MODULE_4___default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(target, key, source[key]); }); } else if (_babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default.a) { _babel_runtime_corejs2_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_1___default()(target, _babel_runtime_corejs2_core_js_object_get_own_property_descriptors__WEBPACK_IMPORTED_MODULE_2___default()(source)); } else { ownKeys(source).forEach(function (key) { _babel_runtime_corejs2_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(target, key, _babel_runtime_corejs2_core_js_object_get_own_property_descriptor__WEBPACK_IMPORTED_MODULE_3___default()(source, key)); }); } } return target; }


 // import { string, object } from 'Joi';




const style = theme => ({
  root: {
    background: `linear-gradient(
            116.83deg,
            #03256c 0%,
            #0846b0 100%,
            #0846b0 100%
          )`,
    color: "white",
    width: "85%"
  },
  //
  form: {
    width: "50%",
    marginLeft: 40,
    marginRight: 40,
    marginTop: 10,
    [theme.breakpoints.down("sm")]: {
      width: "70%"
    }
  },
  //
  loginFrom: {
    width: 500,
    margin: 25,
    paddingTop: 40,
    paddingBottom: 30,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      width: "85%"
    }
  },
  otherSignOptions: {
    display: "flex",
    width: "100%",
    justifyContent: "space-between"
  },
  otherSignButton: {
    padding: 10,
    display: "flex",
    alignItems: "center",
    cursor: "pointer"
  },
  otherSignInImage: {
    marginRight: 10,
    width: 28,
    heigth: 28
  }
});

class SignUpForm extends react__WEBPACK_IMPORTED_MODULE_8___default.a.Component {
  constructor(...args) {
    super(...args);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(this, "state", {
      inputField: {
        email: "",
        code: "+91",
        phone: "",
        password: ""
      },
      error: {},
      userClicked: {
        email: false,
        code: false,
        phone: false,
        password: false
      },
      loginType: "student"
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(this, "validation", inputField => {
      const schema = _hapi_joi__WEBPACK_IMPORTED_MODULE_11__["object"]({
        email: _hapi_joi__WEBPACK_IMPORTED_MODULE_11__["string"]().email({
          minDomainSegments: 2,
          tlds: {
            allow: ["com", "net"]
          }
        }).required(),
        code: _hapi_joi__WEBPACK_IMPORTED_MODULE_11__["string"]().required(),
        phone: _hapi_joi__WEBPACK_IMPORTED_MODULE_11__["string"]().required(),
        password: _hapi_joi__WEBPACK_IMPORTED_MODULE_11__["string"]().min(6).required()
      });
      const options = {
        abortEarly: false
      };
      let error = {};
      let validation = schema.validate(inputField, options);

      if (!validation.error) {
        return error;
      }

      validation.error.details.map(err => {
        error[err.context.key] = err.message;
      });
      return error;
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(this, "handleDisplayError", (totalError, userClicked) => {
      let error = {};

      for (let [key, value] of _babel_runtime_corejs2_core_js_object_entries__WEBPACK_IMPORTED_MODULE_6___default()(totalError)) {
        userClicked[key] ? error[key] = value : null;
      }

      return error;
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(this, "handleInputField", e => {
      const {
        value,
        name
      } = e.currentTarget;

      const userClicked = _objectSpread({}, this.state.userClicked);

      userClicked[name] = true;

      let inputField = _objectSpread({}, this.state.inputField);

      inputField[name] = value;
      this.setState({
        inputField
      });
      let totalError = this.validation(inputField);
      let error = this.handleDisplayError(totalError, userClicked);
      this.setState({
        error,
        userClicked
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(this, "restErrorField", name => {
      let {
        error
      } = this.state;
      error[name] = "";
      this.setState({
        error
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(this, "onBlurField", name => {
      const userClicked = _objectSpread({}, this.state.userClicked);

      userClicked[name] = true;
      console.log(userClicked);
      let totalError = this.validation(this.state.inputField);
      let error = this.handleDisplayError(totalError, userClicked);
      console.log(totalError);
      this.setState({
        error,
        userClicked
      });
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(this, "handleLoginType", (e, value) => {
      this.setState({
        loginType: value
      });
    });
  }

  render() {
    const {
      classes
    } = this.props;
    const {
      email,
      code,
      phone,
      password
    } = this.state.inputField;
    return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Paper"], {
      className: classes.loginFrom,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Typography"], {
      component: "h2",
      variant: "h6",
      color: "primary",
      align: "center",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 148
      },
      __self: this
    }, "Join Vidyalai as a"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Tabs"], {
      value: this.state.loginType,
      onChange: this.handleLoginType,
      indicatorColor: "primary",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Tab"], {
      label: "Student",
      value: "student",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 157
      },
      __self: this
    }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Tab"], {
      label: "Teacher",
      value: "teacher",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 158
      },
      __self: this
    })), __jsx("form", {
      className: classes.form,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160
      },
      __self: this
    }, __jsx(_iconFormInput_iconFormInput_component__WEBPACK_IMPORTED_MODULE_12__["default"], {
      label: "Email",
      name: "email",
      isError: this.state.error.email ? true : false,
      errorVal: this.state.error.email,
      value: email,
      handleInputField: this.handleInputField,
      restErrorField: this.restErrorField,
      onBlurField: this.onBlurField,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 161
      },
      __self: this
    }), __jsx(_iconFormInput_iconFormInput_component__WEBPACK_IMPORTED_MODULE_12__["IconTwoFieldInput"], {
      handleInputField: this.handleInputField,
      value: this.state.inputField,
      error: this.state.error,
      handle: true,
      restErrorField: this.restErrorField,
      onBlurField: this.onBlurField,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 171
      },
      __self: this
    }), __jsx(_iconFormInput_iconFormInput_component__WEBPACK_IMPORTED_MODULE_12__["default"], {
      label: "Password",
      name: "password",
      isError: this.state.error.password ? true : false,
      errorVal: this.state.error.password,
      handleInputField: this.handleInputField,
      value: password,
      restErrorField: this.restErrorField,
      onBlurField: this.onBlurField,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 179
      },
      __self: this
    }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Typography"], {
      variant: "caption",
      component: "h2",
      align: "center",
      style: {
        marginTop: 15
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 189
      },
      __self: this
    }, "By signing up, you accept the Terms and Privacy Policy"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Button"], {
      variant: "contained",
      color: "primary",
      fullWidth: true,
      style: {
        marginTop: 20
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 197
      },
      __self: this
    }, "Create Your Account"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Typography"], {
      variant: "caption",
      component: "h2",
      align: "center",
      style: {
        paddingTop: 15,
        paddingBottom: 20
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 205
      },
      __self: this
    }, "or sign up with"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Box"], {
      className: classes.otherSignOptions,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 213
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Paper"], {
      className: classes.otherSignButton,
      elevation: 5,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 214
      },
      __self: this
    }, __jsx("img", {
      src: "/google-48.png",
      alt: "",
      className: classes.otherSignInImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 215
      },
      __self: this
    }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Typography"], {
      variant: "body2",
      component: "h2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 220
      },
      __self: this
    }, "Google")), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Paper"], {
      className: classes.otherSignButton,
      elevation: 5,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 224
      },
      __self: this
    }, __jsx("img", {
      src: "/facebook.png",
      alt: "",
      className: classes.otherSignInImage,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 225
      },
      __self: this
    }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Typography"], {
      variant: "body2",
      component: "h2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 230
      },
      __self: this
    }, "Facebook"))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__["Typography"], {
      variant: "caption",
      component: "h2",
      align: "center",
      style: {
        paddingTop: 15,
        paddingBottom: 20
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 235
      },
      __self: this
    }, "Already have an account? Login")));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__["withStyles"])(style)(SignUpForm));

/***/ }),

/***/ "./components/slider/TeacherSlider.component.jsx":
/*!*******************************************************!*\
  !*** ./components/slider/TeacherSlider.component.jsx ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-slick */ "react-slick");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/icons/ChevronLeft */ "@material-ui/icons/ChevronLeft");
/* harmony import */ var _material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/ChevronRight */ "@material-ui/icons/ChevronRight");
/* harmony import */ var _material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _TeacherSlider_style_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./TeacherSlider.style.scss */ "./components/slider/TeacherSlider.style.scss");
/* harmony import */ var _TeacherSlider_style_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_TeacherSlider_style_scss__WEBPACK_IMPORTED_MODULE_7__);

var _jsxFileName = "/home/bharath/company/assignment1/components/slider/TeacherSlider.component.jsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;








const style = theme => ({
  root: {
    position: "relative",
    margin: "auto",
    width: "50%",
    [theme.breakpoints.down("sm")]: {
      width: "70%"
    }
  },
  item: {
    position: "relative",
    alignSelf: "center",
    margin: "auto",
    textAlign: "center"
  },
  //
  img: {
    margin: "auto",
    width: 100,
    height: 100,
    borderRadius: "50%",
    transition: "0.5s"
  },
  nextButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    transfrom: "translateY(-50%)",
    left: -40,
    zIndex: 2,
    background: "white"
  },
  previousButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    right: -40,
    transfrom: "translateY(-50%)",
    zIndex: 2,
    background: "white"
  }
});

class SimpleSlider extends react__WEBPACK_IMPORTED_MODULE_1__["Component"] {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  next() {
    this.slider.slickNext();
  }

  previous() {
    this.slider.slickPrev();
  }

  render() {
    const settings = {
      className: "center-content",
      centerMode: true,
      infinite: true,
      centerPadding: "5px",
      slidesToShow: this.props.isSmall ? 1 : 3,
      speed: 500
    };
    const {
      classes
    } = this.props;
    return __jsx("div", {
      className: classes.root,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Fab"], {
      size: "small",
      onClick: this.next,
      className: classes.nextButton,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: this
    }, __jsx(_material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Box"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, __jsx(react_slick__WEBPACK_IMPORTED_MODULE_2___default.a, Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, settings, {
      ref: c => this.slider = c,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    }), this.props.teacher.map((slide, idx) => __jsx("div", {
      className: classes.item,
      key: idx,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Box"], {
      className: "slide-content-wrapper",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }), __jsx("img", {
      src: slide.imgUrl,
      alt: "",
      className: classes.img,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84
      },
      __self: this
    }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
      component: "h2",
      variant: "h6",
      className: "slide-content-name",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, slide.name), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
      component: "h2",
      variant: "body1",
      className: "slide-content-qualification",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 92
      },
      __self: this
    }, slide.qualification), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
      component: "h2",
      variant: "subtitle1",
      className: "slide-content-special",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99
      },
      __self: this
    }, slide.special))))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Fab"], {
      size: "small",
      onClick: this.previous,
      className: classes.previousButton,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 111
      },
      __self: this
    }, __jsx(_material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 116
      },
      __self: this
    })));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__["withStyles"])(style)(SimpleSlider));

/***/ }),

/***/ "./components/slider/TeacherSlider.style.scss":
/*!****************************************************!*\
  !*** ./components/slider/TeacherSlider.style.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/teachersContainer/teacherContainer.component.jsx":
/*!*********************************************************************!*\
  !*** ./components/teachersContainer/teacherContainer.component.jsx ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _slider_TeacherSlider_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../slider/TeacherSlider.component */ "./components/slider/TeacherSlider.component.jsx");
/* harmony import */ var _teachers_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teachers.data */ "./components/teachersContainer/teachers.data.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/bharath/company/assignment1/components/teachersContainer/teacherContainer.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  root: {
    height: 550
  },
  title: {
    color: "blue",
    fontWeight: 500,
    padding: "50px 50px 0 50px"
  },
  subTitle: {
    padding: 10,
    paddingBottom: 50
  }
}));

const TeachersContainers = ({
  isSmall
}) => {
  console.log(isSmall);
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Box"], {
    className: classes.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Box"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Typography"], {
    variant: "h5",
    component: "h2",
    align: "center",
    className: classes.title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: undefined
  }, "Meet ours Teachers"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_4__["Typography"], {
    variant: "body1",
    component: "h2",
    align: "center",
    className: classes.subTitle,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: undefined
  }, "Expert teachers from top universities will guide you in all your academic needs")), __jsx(_slider_TeacherSlider_component__WEBPACK_IMPORTED_MODULE_2__["default"], {
    teacher: _teachers_data__WEBPACK_IMPORTED_MODULE_3__["teachers"],
    isSmall: isSmall,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: undefined
  }), ";");
};

/* harmony default export */ __webpack_exports__["default"] = (TeachersContainers);

/***/ }),

/***/ "./components/teachersContainer/teachers.data.js":
/*!*******************************************************!*\
  !*** ./components/teachersContainer/teachers.data.js ***!
  \*******************************************************/
/*! exports provided: teachers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "teachers", function() { return teachers; });
const teachers = [{
  name: "Anzifar Abdul Latheef",
  qualification: "BTech,IIT Madras",
  special: "Resident Polymath",
  imgUrl: "/teacher_anzifar.jpg"
}, {
  name: "Geetha",
  qualification: "BS, IISC & MS, UPJV France",
  special: "Chemistry Wizard",
  imgUrl: "/teacher_srivatsank.jpg"
}, {
  name: "Ajay S Dinesh",
  qualification: "BTech, IIT Madras",
  special: "Math Mage",
  imgUrl: "/teacher_ajay.jpg"
}, {
  name: "Vinayak Vinod",
  qualification: "BS/MS Physics , IIT Madras",
  special: "Physics Pundit ",
  imgUrl: "/teacher_vinayak.jpg"
}, {
  name: "Rabia Reshmani",
  qualification: "MS, IIT Bombay",
  special: "Mathematics",
  imgUrl: "/teacher_rabia.png"
}, {
  name: "Laghavi Sharma",
  qualification: "A2, Spanish, Instituto de Cervantes",
  special: "Spanish",
  imgUrl: "/teacher_laghavi.png"
}];

/***/ }),

/***/ "./components/testimonialContainer/leftQuote.jsx":
/*!*******************************************************!*\
  !*** ./components/testimonialContainer/leftQuote.jsx ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/bharath/company/assignment1/components/testimonialContainer/leftQuote.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

const LeftQuote = () => {
  return __jsx("svg", {
    width: "33",
    height: "30",
    viewBox: "0 0 33 30",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    },
    __self: undefined
  }, __jsx("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M23.9807 14.9165C23.9807 15.9772 24.6105 16.5407 25.8701 16.6069L27.8589 16.7064C29.1848 16.7727 30.3615 17.4025 31.389 18.5958C32.4166 19.789 32.9303 21.1481 32.9303 22.6728C32.9303 24.529 32.2674 26.0869 30.9415 27.3465C29.6156 28.6061 27.9583 29.2359 25.9695 29.2359C23.6492 29.2359 21.7433 28.3243 20.2517 26.5013C18.7601 24.6782 18.0143 22.3414 18.0143 19.4907C18.0143 14.1872 20.1025 8.75119 24.279 3.1825C25.8038 1.12739 27.1296 0.0998535 28.2566 0.0998535C29.5162 0.0998535 30.146 0.762782 30.146 2.08866C30.146 2.61901 29.7814 3.34823 29.0522 4.27635C27.2622 6.53034 25.7706 9.2152 24.5773 12.331C24.1796 13.3917 23.9807 14.2535 23.9807 14.9165ZM5.88261 14.9165C5.88261 15.9772 6.51239 16.5407 7.77197 16.6069L9.76078 16.7064C11.153 16.7727 12.3628 17.4025 13.3904 18.5958C14.4179 19.789 14.9317 21.1481 14.9317 22.6728C14.9317 24.529 14.2687 26.0869 12.9429 27.3465C11.617 28.6061 9.95967 29.2359 7.97085 29.2359C5.65057 29.2359 3.74465 28.3243 2.25303 26.5013C0.76142 24.6782 0.015625 22.3414 0.015625 19.4907C0.015625 14.1872 2.10385 8.75119 6.28037 3.1825C7.80513 1.12739 9.13098 0.0998535 10.258 0.0998535C11.5176 0.0998535 12.1473 0.762782 12.1473 2.08866C12.1473 2.61901 11.7827 3.34823 11.0535 4.27635C9.72762 5.93369 8.53435 7.85619 7.47365 10.0439C6.41295 12.2316 5.88261 13.8558 5.88261 14.9165Z",
    fill: "#3ECF8E",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (LeftQuote);

/***/ }),

/***/ "./components/testimonialContainer/testimonial.data.js":
/*!*************************************************************!*\
  !*** ./components/testimonialContainer/testimonial.data.js ***!
  \*************************************************************/
/*! exports provided: testimonialData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testimonialData", function() { return testimonialData; });
const testimonialData = {
  testimonials: [{
    name: "Vinish Pal Singh",
    imgUrl: "/vansh.jpg",
    type: "Parent of Vansh IB Grade 10",
    quote: "This is an excellent way of teaching kids at the convenience of your home. Also the quality of teachers is very good . The support I received from Ms Shaheem was excellent. My kids performed better with their guidance . Thanks Vidyalai."
  }, {
    name: "Anil Nair",
    imgUrl: "/anil.jpg",
    type: "Parent",
    quote: "They have some of the best Teachers and all of them IITians and bright minds.My son is so happy with the teaching and I have also referred this to some of my colleagues.I would recommend this to any one who is looking for high quality teaching."
  }, {
    name: "Dr. Vrishali Patil",
    imgUrl: "/vrishali.png",
    type: "Parent",
    quote: "My son Advait has been enrolled in Vidyalai for about 4 months, and he finds it very helpful in school. Coming from the US, it was hard finding tutoring for the International Baccalaureate until we learned about Vidyalai. As the classes were held online, it was very convenient and saves travel time. With Vidyalai, Advait was able to get a head start in his academics and got a grasp of the Math, Physics, and Chemistry topics. I would recommend Vidyalai to any parent looking for online tutoring."
  }, {
    name: "Siddharth",
    imgUrl: "/siddharth.jpg",
    type: "Student",
    quote: "We are very pleased with the kind of interaction that has been made possible between the student and the tutor which we believe has led to a clear understanding of various concepts that are dealt with."
  }]
};

/***/ }),

/***/ "./components/testimonialContainer/testimonialContainer.component.jsx":
/*!****************************************************************************!*\
  !*** ./components/testimonialContainer/testimonialContainer.component.jsx ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _testimonialSlider_testimonialSlider_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../testimonialSlider/testimonialSlider.component */ "./components/testimonialSlider/testimonialSlider.component.jsx");
/* harmony import */ var _testimonial_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./testimonial.data */ "./components/testimonialContainer/testimonial.data.js");
var _jsxFileName = "/home/bharath/company/assignment1/components/testimonialContainer/testimonialContainer.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const useStyle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  root: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    flexDirection: "column",
    margin: "auto",
    background: "#E9F2FC",
    height: 600,
    [theme.breakpoints.down("md")]: {
      paddingTop: 70,
      height: 900
    }
  },
  title: {
    paddingTop: 50,
    paddingBottom: 10,
    fontWeight: 600
  }
}));

const TestimonialContainer = () => {
  const classes = useStyle();
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    className: classes.root,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h5",
    component: "h2",
    color: "primary",
    align: "center",
    className: classes.title,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: undefined
  }, "Testimonial"), __jsx(_testimonialSlider_testimonialSlider_component__WEBPACK_IMPORTED_MODULE_3__["default"], {
    testimonials: _testimonial_data__WEBPACK_IMPORTED_MODULE_4__["testimonialData"].testimonials,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: undefined
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (TestimonialContainer);

/***/ }),

/***/ "./components/testimonialSlider/testimonialSlider.component.jsx":
/*!**********************************************************************!*\
  !*** ./components/testimonialSlider/testimonialSlider.component.jsx ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-slick */ "react-slick");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/icons/ChevronLeft */ "@material-ui/icons/ChevronLeft");
/* harmony import */ var _material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/ChevronRight */ "@material-ui/icons/ChevronRight");
/* harmony import */ var _material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _testimonialContainer_leftQuote__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../testimonialContainer/leftQuote */ "./components/testimonialContainer/leftQuote.jsx");

var _jsxFileName = "/home/bharath/company/assignment1/components/testimonialSlider/testimonialSlider.component.jsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;








const style = theme => ({
  root: {
    position: "relative",
    margin: "auto",
    width: "65%"
  },
  cardWrapper: {
    width: "96%",
    // height: "50%",
    borderRadius: 20,
    marginLeft: 7
  },
  userProfileContainer: {
    background: `linear-gradient(326.59deg, #45CFE2 -1.77%, #2196F3 51.27%, #3959FA 100%)`,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    color: "white",
    [theme.breakpoints.down("sm")]: {
      // display: "none",
      borderRadius: 20
    }
  },
  //
  avatar: {
    width: 125,
    height: 125,
    borderRadius: "50%",
    backgroundSize: "contain"
  },
  userQuoteContainer: {
    // height: 500,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "column",
    padding: "30px 40px 40px 20px"
  },
  leftQuote: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 85,
    height: 85,
    borderRadius: "50%",
    background: "#D6FACF",
    marginBottom: 50
  },
  quote: {
    marginBottom: 35
  },
  nextButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    transfrom: "translateY(-50%)",
    left: -60,
    zIndex: 2,
    background: "white"
  },
  previousButton: {
    color: "blue",
    position: "absolute",
    top: 135,
    right: -60,
    transfrom: "translateY(-50%)",
    zIndex: 2,
    background: "white"
  }
});

class TestimonialSlider extends react__WEBPACK_IMPORTED_MODULE_1__["Component"] {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  next() {
    this.slider.slickNext();
  }

  previous() {
    this.slider.slickPrev();
  }

  render() {
    const settings = {
      className: "center-content",
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerPadding: "5px"
    };
    const {
      classes
    } = this.props;
    return __jsx("div", {
      className: classes.root,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Fab"], {
      size: "small",
      onClick: this.next,
      className: classes.nextButton,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, __jsx(_material_ui_icons_ChevronLeft__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119
      },
      __self: this
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Box"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 121
      },
      __self: this
    }, __jsx(react_slick__WEBPACK_IMPORTED_MODULE_2___default.a, Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, settings, {
      ref: c => this.slider = c,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }), this.props.testimonials.map((value, idx) => __jsx("div", {
      className: classes.item,
      key: idx,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 124
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Paper"], {
      className: classes.cardWrapper,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grid"], {
      container: true,
      className: classes.container,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grid"], {
      item: true,
      className: classes.userProfileContainer,
      md: 4,
      xs: 12,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 127
      },
      __self: this
    }, __jsx("div", {
      style: {
        backgroundImage: `url(${value.imgUrl})`
      },
      className: classes.avatar,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133
      },
      __self: this
    }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
      variant: "h6",
      component: "h2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }, value.name), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
      variant: "body1",
      component: "h2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 140
      },
      __self: this
    }, value.type)), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Grid"], {
      item: true,
      md: 8,
      sm: 12,
      className: classes.userQuoteContainer,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144
      },
      __self: this
    }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Box"], {
      className: classes.leftQuote,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150
      },
      __self: this
    }, __jsx(_testimonialContainer_leftQuote__WEBPACK_IMPORTED_MODULE_7__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    })), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Typography"], {
      variant: "body1",
      component: "h2",
      className: classes.quote,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 153
      },
      __self: this
    }, "\"", value.quote, "\""), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Button"], {
      variant: "text",
      color: "primary",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160
      },
      __self: this
    }, "Read more on Google")))))))), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__["Fab"], {
      size: "small",
      onClick: this.previous,
      className: classes.previousButton,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 171
      },
      __self: this
    }, __jsx(_material_ui_icons_ChevronRight__WEBPACK_IMPORTED_MODULE_4___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 176
      },
      __self: this
    })));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_6__["withStyles"])(style)(TestimonialSlider));

/***/ }),

/***/ "./components/whyContainer/why.data.js":
/*!*********************************************!*\
  !*** ./components/whyContainer/why.data.js ***!
  \*********************************************/
/*! exports provided: whyData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "whyData", function() { return whyData; });
const whyData = [{
  imgUrl: "/learnFromAnywhere.svg",
  title: "Learn From AnyWhere",
  content: "Attend classes from anywhere in the world, from the conveinence of your own home. You don’t have to travel anywhtere."
}, {
  imgUrl: "/personalAttention.svg",
  title: "Personal Attention",
  content: "Each student will be taught and mentored by a single teacher to personalise the learning experience."
}, {
  imgUrl: "/safeAndConvenient.svg",
  title: "Safe and Convenient",
  content: "Learn from the safety of your home, at any convenient time, without any of the hassles of travel."
}];

/***/ }),

/***/ "./components/whyContainer/whyContainer.component.jsx":
/*!************************************************************!*\
  !*** ./components/whyContainer/whyContainer.component.jsx ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _why_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./why.data */ "./components/whyContainer/why.data.js");
var _jsxFileName = "/home/bharath/company/assignment1/components/whyContainer/whyContainer.component.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const useStyle = theme => ({
  root: {}
}); //


const whyContainer = () => {
  return __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    paddingBottom: "25px",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h5",
    component: "h2",
    align: "center",
    style: {
      padding: 40,
      fontWeight: 500
    },
    color: "primary",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: undefined
  }, "Why Vidyalai?"), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    container: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: undefined
  }, _why_data__WEBPACK_IMPORTED_MODULE_3__["whyData"].map((val, idx) => __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Grid"], {
    items: true,
    key: idx,
    md: 4,
    sm: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: undefined
  }, __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Box"], {
    display: "flex",
    justifyContent: "space-around",
    flexDirection: "column",
    style: {
      padding: 30
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: undefined
  }, __jsx("img", {
    src: val.imgUrl,
    alt: "",
    style: {
      paddingBottom: 15
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: undefined
  }), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "h5",
    gutterBottom: true,
    align: "center",
    color: "primary",
    style: {
      fontWeight: 500
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: undefined
  }, val.title), __jsx(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__["Typography"], {
    variant: "body1",
    gutterBottom: true,
    align: "center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: undefined
  }, val.content))))));
};

/* harmony default export */ __webpack_exports__["default"] = (whyContainer);

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/assign.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/assign */ "core-js/library/fn/object/assign");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-properties */ "core-js/library/fn/object/define-properties");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/entries.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/entries.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/entries */ "core-js/library/fn/object/entries");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js":
/*!*******************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptor */ "core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js":
/*!********************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-descriptors */ "core-js/library/fn/object/get-own-property-descriptors");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-own-property-symbols */ "core-js/library/fn/object/get-own-property-symbols");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/keys.js":
/*!********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/keys */ "core-js/library/fn/object/keys");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js":
/*!********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _extends; });
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__);

function _extends() {
  _extends = _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default.a || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/***/ }),

/***/ "./pages/index.jsx":
/*!*************************!*\
  !*** ./pages/index.jsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/defineProperty */ "./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_introContainer_introContainer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/introContainer/introContainer.component */ "./components/introContainer/introContainer.component.jsx");
/* harmony import */ var _components_briefContainer_briefContainer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/briefContainer/briefContainer.component */ "./components/briefContainer/briefContainer.component.jsx");
/* harmony import */ var _components_lessonRequestContainer_lessonRequestContainer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/lessonRequestContainer/lessonRequestContainer */ "./components/lessonRequestContainer/lessonRequestContainer.jsx");
/* harmony import */ var _components_teachersContainer_teacherContainer_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/teachersContainer/teacherContainer.component */ "./components/teachersContainer/teacherContainer.component.jsx");
/* harmony import */ var _components_testimonialContainer_testimonialContainer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/testimonialContainer/testimonialContainer.component */ "./components/testimonialContainer/testimonialContainer.component.jsx");
/* harmony import */ var _components_signUpForm_signUpForm_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/signUpForm/signUpForm.component */ "./components/signUpForm/signUpForm.component.jsx");
/* harmony import */ var _components_signUpContainer_signUpContainer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/signUpContainer/signUpContainer.component */ "./components/signUpContainer/signUpContainer.component.jsx");
/* harmony import */ var _components_footerContainer_footerContainer_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/footerContainer/footerContainer.component */ "./components/footerContainer/footerContainer.component.jsx");
/* harmony import */ var _components_whyContainer_whyContainer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/whyContainer/whyContainer.component */ "./components/whyContainer/whyContainer.component.jsx");

var _jsxFileName = "/home/bharath/company/assignment1/pages/index.jsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement;












class Index extends react__WEBPACK_IMPORTED_MODULE_2___default.a.Component {
  constructor(...args) {
    super(...args);

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "state", {
      scrollPosition: 0,
      innerWidth: 0
    });

    Object(_babel_runtime_corejs2_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(this, "handleScroll", () => {
      let scrollVal = window.pageYOffset;
      this.setState({
        scrollPosition: scrollVal
      });
    });
  }

  componentDidMount() {
    this.setState({
      innerWidth: window.innerWidth
    });
    window.addEventListener("scroll", this.handleScroll);
  } //


  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  render() {
    return __jsx("div", {
      className: "jsx-1844624174",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, __jsx("link", {
      rel: "stylesheet",
      href: "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap",
      className: "jsx-1844624174",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }), __jsx("link", {
      rel: "stylesheet",
      type: "text/css",
      charSet: "UTF-8",
      href: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css",
      className: "jsx-1844624174",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }), __jsx("link", {
      rel: "stylesheet",
      type: "text/css",
      href: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css",
      className: "jsx-1844624174",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    })), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default.a, {
      id: "1844624174",
      __self: this
    }, "body{height:\"100%\";width:\"100%\";margin:0;padding:0;font-family:\"Roboto', sans-serif\";}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2JoYXJhdGgvY29tcGFueS9hc3NpZ25tZW50MS9wYWdlcy9pbmRleC5qc3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBaUQyQixBQUcyQixjQUNELGFBQ0osU0FDQyxVQUN3QixrQ0FDcEMiLCJmaWxlIjoiL2hvbWUvYmhhcmF0aC9jb21wYW55L2Fzc2lnbm1lbnQxL3BhZ2VzL2luZGV4LmpzeCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcbmltcG9ydCBJbnRyb1BhZ2UgZnJvbSBcIi4uL2NvbXBvbmVudHMvaW50cm9Db250YWluZXIvaW50cm9Db250YWluZXIuY29tcG9uZW50XCI7XG5pbXBvcnQgQnJpZWZDb250YWluZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvYnJpZWZDb250YWluZXIvYnJpZWZDb250YWluZXIuY29tcG9uZW50XCI7XG5pbXBvcnQgTGVzc29uUmVxdWVzdENvbnRhaW5lciBmcm9tIFwiLi4vY29tcG9uZW50cy9sZXNzb25SZXF1ZXN0Q29udGFpbmVyL2xlc3NvblJlcXVlc3RDb250YWluZXJcIjtcbmltcG9ydCBUZWFjaGVyc0NvbnRhaW5lcnMgZnJvbSBcIi4uL2NvbXBvbmVudHMvdGVhY2hlcnNDb250YWluZXIvdGVhY2hlckNvbnRhaW5lci5jb21wb25lbnRcIjtcbmltcG9ydCBUZXN0aW1vbmlhbENvbnRhaW5lciBmcm9tIFwiLi4vY29tcG9uZW50cy90ZXN0aW1vbmlhbENvbnRhaW5lci90ZXN0aW1vbmlhbENvbnRhaW5lci5jb21wb25lbnRcIjtcbmltcG9ydCBTaWduVXBGb3JtQ29tcG9uZW50IGZyb20gXCIuLi9jb21wb25lbnRzL3NpZ25VcEZvcm0vc2lnblVwRm9ybS5jb21wb25lbnRcIjtcbmltcG9ydCBTaWduVXBDb250YWluZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvc2lnblVwQ29udGFpbmVyL3NpZ25VcENvbnRhaW5lci5jb21wb25lbnRcIjtcbmltcG9ydCBGb290ZXJDb250YWluZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvZm9vdGVyQ29udGFpbmVyL2Zvb3RlckNvbnRhaW5lci5jb21wb25lbnRcIjtcbmltcG9ydCBXaHlDb250YWluZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvd2h5Q29udGFpbmVyL3doeUNvbnRhaW5lci5jb21wb25lbnRcIjtcbmNsYXNzIEluZGV4IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGUgPSB7XG4gICAgc2Nyb2xsUG9zaXRpb246IDAsXG4gICAgaW5uZXJXaWR0aDogMFxuICB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBpbm5lcldpZHRoOiB3aW5kb3cuaW5uZXJXaWR0aCB9KTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLCB0aGlzLmhhbmRsZVNjcm9sbCk7XG4gIH1cbiAgLy9cbiAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIiwgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICB9XG4gIGhhbmRsZVNjcm9sbCA9ICgpID0+IHtcbiAgICBsZXQgc2Nyb2xsVmFsID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xuICAgIHRoaXMuc2V0U3RhdGUoeyBzY3JvbGxQb3NpdGlvbjogc2Nyb2xsVmFsIH0pO1xuICB9O1xuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxIZWFkPlxuICAgICAgICAgIDxsaW5rXG4gICAgICAgICAgICByZWw9XCJzdHlsZXNoZWV0XCJcbiAgICAgICAgICAgIGhyZWY9XCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9Um9ib3RvOjMwMCw0MDAsNTAwLDcwMCZkaXNwbGF5PXN3YXBcIlxuICAgICAgICAgIC8+XG4gICAgICAgICAgPGxpbmtcbiAgICAgICAgICAgIHJlbD1cInN0eWxlc2hlZXRcIlxuICAgICAgICAgICAgdHlwZT1cInRleHQvY3NzXCJcbiAgICAgICAgICAgIGNoYXJTZXQ9XCJVVEYtOFwiXG4gICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly9jZG5qcy5jbG91ZGZsYXJlLmNvbS9hamF4L2xpYnMvc2xpY2stY2Fyb3VzZWwvMS42LjAvc2xpY2subWluLmNzc1wiXG4gICAgICAgICAgLz5cbiAgICAgICAgICA8bGlua1xuICAgICAgICAgICAgcmVsPVwic3R5bGVzaGVldFwiXG4gICAgICAgICAgICB0eXBlPVwidGV4dC9jc3NcIlxuICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vY2RuanMuY2xvdWRmbGFyZS5jb20vYWpheC9saWJzL3NsaWNrLWNhcm91c2VsLzEuNi4wL3NsaWNrLXRoZW1lLm1pbi5jc3NcIlxuICAgICAgICAgIC8+XG4gICAgICAgIDwvSGVhZD5cbiAgICAgICAgPHN0eWxlIGpzeCBnbG9iYWw+e2BcbiAgICAgICAgICBib2R5IHtcbiAgICAgICAgICAgIGhlaWdodDogXCIxMDAlXCI7XG4gICAgICAgICAgICB3aWR0aDogXCIxMDAlXCI7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvJywgc2Fucy1zZXJpZlwiO1xuICAgICAgICAgIH1cbiAgICAgICAgYH08L3N0eWxlPlxuICAgICAgICA8SW50cm9QYWdlXG4gICAgICAgICAgaXNTY3JvbGxlZD17dGhpcy5zdGF0ZS5zY3JvbGxQb3NpdGlvbiA8IDIwID8gdHJ1ZSA6IGZhbHNlfVxuICAgICAgICA+PC9JbnRyb1BhZ2U+XG4gICAgICAgIDxCcmllZkNvbnRhaW5lcj48L0JyaWVmQ29udGFpbmVyPlxuICAgICAgICA8TGVzc29uUmVxdWVzdENvbnRhaW5lcj48L0xlc3NvblJlcXVlc3RDb250YWluZXI+XG4gICAgICAgIDxUZWFjaGVyc0NvbnRhaW5lcnNcbiAgICAgICAgICBpc1NtYWxsPXt0aGlzLnN0YXRlLmlubmVyV2lkdGggPCA4MTUgPyB0cnVlIDogZmFsc2V9XG4gICAgICAgID48L1RlYWNoZXJzQ29udGFpbmVycz5cbiAgICAgICAgPFRlc3RpbW9uaWFsQ29udGFpbmVyPjwvVGVzdGltb25pYWxDb250YWluZXI+XG4gICAgICAgIHsvKiA8UmVxdWVzdExlc3Nvbj48L1JlcXVlc3RMZXNzb24+ICovfVxuICAgICAgICB7LyogPFNpZ25VcEZvcm0+PC9TaWduVXBGb3JtPiAqL31cblxuICAgICAgICA8V2h5Q29udGFpbmVyPjwvV2h5Q29udGFpbmVyPlxuICAgICAgICA8U2lnblVwQ29udGFpbmVyPjwvU2lnblVwQ29udGFpbmVyPlxuICAgICAgICA8Rm9vdGVyQ29udGFpbmVyPjwvRm9vdGVyQ29udGFpbmVyPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuZXhwb3J0IGRlZmF1bHQgSW5kZXg7XG4iXX0= */\n/*@ sourceURL=/home/bharath/company/assignment1/pages/index.jsx */"), __jsx(_components_introContainer_introContainer_component__WEBPACK_IMPORTED_MODULE_4__["default"], {
      isScrolled: this.state.scrollPosition < 20 ? true : false,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }), __jsx(_components_briefContainer_briefContainer_component__WEBPACK_IMPORTED_MODULE_5__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }), __jsx(_components_lessonRequestContainer_lessonRequestContainer__WEBPACK_IMPORTED_MODULE_6__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: this
    }), __jsx(_components_teachersContainer_teacherContainer_component__WEBPACK_IMPORTED_MODULE_7__["default"], {
      isSmall: this.state.innerWidth < 815 ? true : false,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }), __jsx(_components_testimonialContainer_testimonialContainer_component__WEBPACK_IMPORTED_MODULE_8__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }), __jsx(_components_whyContainer_whyContainer_component__WEBPACK_IMPORTED_MODULE_12__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }), __jsx(_components_signUpContainer_signUpContainer_component__WEBPACK_IMPORTED_MODULE_10__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }), __jsx(_components_footerContainer_footerContainer_component__WEBPACK_IMPORTED_MODULE_11__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73
      },
      __self: this
    }));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ 4:
/*!*******************************!*\
  !*** multi ./pages/index.jsx ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/bharath/company/assignment1/pages/index.jsx */"./pages/index.jsx");


/***/ }),

/***/ "@hapi/joi":
/*!****************************!*\
  !*** external "@hapi/joi" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@hapi/joi");

/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core");

/***/ }),

/***/ "@material-ui/core/Button":
/*!*******************************************!*\
  !*** external "@material-ui/core/Button" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Button");

/***/ }),

/***/ "@material-ui/core/styles":
/*!*******************************************!*\
  !*** external "@material-ui/core/styles" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ "@material-ui/icons/AccountCircle":
/*!***************************************************!*\
  !*** external "@material-ui/icons/AccountCircle" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/AccountCircle");

/***/ }),

/***/ "@material-ui/icons/ArrowDropDown":
/*!***************************************************!*\
  !*** external "@material-ui/icons/ArrowDropDown" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ArrowDropDown");

/***/ }),

/***/ "@material-ui/icons/ArrowForward":
/*!**************************************************!*\
  !*** external "@material-ui/icons/ArrowForward" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ArrowForward");

/***/ }),

/***/ "@material-ui/icons/ArrowRight":
/*!************************************************!*\
  !*** external "@material-ui/icons/ArrowRight" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ArrowRight");

/***/ }),

/***/ "@material-ui/icons/ChevronLeft":
/*!*************************************************!*\
  !*** external "@material-ui/icons/ChevronLeft" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ChevronLeft");

/***/ }),

/***/ "@material-ui/icons/ChevronRight":
/*!**************************************************!*\
  !*** external "@material-ui/icons/ChevronRight" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ChevronRight");

/***/ }),

/***/ "@material-ui/icons/CollectionsBookmark":
/*!*********************************************************!*\
  !*** external "@material-ui/icons/CollectionsBookmark" ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/CollectionsBookmark");

/***/ }),

/***/ "@material-ui/icons/ContactSupport":
/*!****************************************************!*\
  !*** external "@material-ui/icons/ContactSupport" ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ContactSupport");

/***/ }),

/***/ "@material-ui/icons/Drafts":
/*!********************************************!*\
  !*** external "@material-ui/icons/Drafts" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Drafts");

/***/ }),

/***/ "@material-ui/icons/ExpandLess":
/*!************************************************!*\
  !*** external "@material-ui/icons/ExpandLess" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ExpandLess");

/***/ }),

/***/ "@material-ui/icons/ExpandMore":
/*!************************************************!*\
  !*** external "@material-ui/icons/ExpandMore" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/ExpandMore");

/***/ }),

/***/ "@material-ui/icons/Facebook":
/*!**********************************************!*\
  !*** external "@material-ui/icons/Facebook" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Facebook");

/***/ }),

/***/ "@material-ui/icons/Instagram":
/*!***********************************************!*\
  !*** external "@material-ui/icons/Instagram" ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Instagram");

/***/ }),

/***/ "@material-ui/icons/LibraryBooks":
/*!**************************************************!*\
  !*** external "@material-ui/icons/LibraryBooks" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/LibraryBooks");

/***/ }),

/***/ "@material-ui/icons/LinkedIn":
/*!**********************************************!*\
  !*** external "@material-ui/icons/LinkedIn" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/LinkedIn");

/***/ }),

/***/ "@material-ui/icons/Lock":
/*!******************************************!*\
  !*** external "@material-ui/icons/Lock" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Lock");

/***/ }),

/***/ "@material-ui/icons/Mail":
/*!******************************************!*\
  !*** external "@material-ui/icons/Mail" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Mail");

/***/ }),

/***/ "@material-ui/icons/Menu":
/*!******************************************!*\
  !*** external "@material-ui/icons/Menu" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Menu");

/***/ }),

/***/ "@material-ui/icons/MoveToInbox":
/*!*************************************************!*\
  !*** external "@material-ui/icons/MoveToInbox" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/MoveToInbox");

/***/ }),

/***/ "@material-ui/icons/PersonAdd":
/*!***********************************************!*\
  !*** external "@material-ui/icons/PersonAdd" ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/PersonAdd");

/***/ }),

/***/ "@material-ui/icons/Phone":
/*!*******************************************!*\
  !*** external "@material-ui/icons/Phone" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Phone");

/***/ }),

/***/ "@material-ui/icons/Send":
/*!******************************************!*\
  !*** external "@material-ui/icons/Send" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Send");

/***/ }),

/***/ "@material-ui/icons/Twitter":
/*!*********************************************!*\
  !*** external "@material-ui/icons/Twitter" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Twitter");

/***/ }),

/***/ "@material-ui/icons/VerifiedUser":
/*!**************************************************!*\
  !*** external "@material-ui/icons/VerifiedUser" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/VerifiedUser");

/***/ }),

/***/ "core-js/library/fn/object/assign":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/assign" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "core-js/library/fn/object/define-properties":
/*!**************************************************************!*\
  !*** external "core-js/library/fn/object/define-properties" ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-properties");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/entries":
/*!****************************************************!*\
  !*** external "core-js/library/fn/object/entries" ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/entries");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptor":
/*!************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptor" ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-descriptors":
/*!*************************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-descriptors" ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptors");

/***/ }),

/***/ "core-js/library/fn/object/get-own-property-symbols":
/*!*********************************************************************!*\
  !*** external "core-js/library/fn/object/get-own-property-symbols" ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-symbols");

/***/ }),

/***/ "core-js/library/fn/object/keys":
/*!*************************************************!*\
  !*** external "core-js/library/fn/object/keys" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-slick":
/*!******************************!*\
  !*** external "react-slick" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-slick");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map